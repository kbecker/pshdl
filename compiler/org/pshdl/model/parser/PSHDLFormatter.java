package org.pshdl.model.parser;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.Token;

public class PSHDLFormatter {
    private static final Set<Integer> spacing = new HashSet<>();
    private static final Integer[] ASSIGNMENTS = { PSHDLLangLexer.ASSGN, PSHDLLangLexer.ADD_ASSGN, PSHDLLangLexer.AND_ASSGN, PSHDLLangLexer.DIV_ASSGN, PSHDLLangLexer.MOD_ASSGN,
            PSHDLLangLexer.MUL_ASSGN, PSHDLLangLexer.OR_ASSGN, PSHDLLangLexer.SLL_ASSGN, PSHDLLangLexer.SRA_ASSGN, PSHDLLangLexer.SRL_ASSGN, PSHDLLangLexer.SUB_ASSGN,
            PSHDLLangLexer.XOR_ASSGN };
    private static final Integer[] COMPARISONS = { PSHDLLangLexer.GREATER_EQ, PSHDLLangLexer.EQ, PSHDLLangLexer.LESS_EQ, PSHDLLangLexer.NOT_EQ };
    private static final Integer[] OPS = { PSHDLLangLexer.AND, PSHDLLangLexer.DIV, PSHDLLangLexer.MUL, PSHDLLangLexer.MOD, PSHDLLangLexer.OR, PSHDLLangLexer.PLUS,
            PSHDLLangLexer.POW, PSHDLLangLexer.SLL, PSHDLLangLexer.SRA, PSHDLLangLexer.SRL, PSHDLLangLexer.XOR };
    private static final Integer[] OTHER_OPS = { PSHDLLangLexer.HASH };
    private static final Integer[] WHITESPACE = { PSHDLLangLexer.RULE_ML_COMMENT, PSHDLLangLexer.RULE_SL_COMMENT, PSHDLLangLexer.RULE_WS };

    public static void main(String[] args) throws IOException {
        final Set<Integer> spacing = new HashSet<>();
        final Set<Integer> whitespace = new HashSet<>(Arrays.asList(WHITESPACE));
        spacing.addAll(Arrays.asList(ASSIGNMENTS));
        spacing.addAll(Arrays.asList(OPS));
        spacing.addAll(Arrays.asList(OTHER_OPS));
        spacing.addAll(Arrays.asList(COMPARISONS));
        final CharStream stream = CharStreams.fromFileName(args[0]);
        final PSHDLLangLexer lexer = new PSHDLLangLexer(stream);
        spacing.addAll(Arrays.asList());
        Token token = null;
        int lastToken = -1;
        while (((token = lexer.nextToken()) != null) && (!lexer._hitEOF)) {
            final int currentToken = token.getType();
            if (!whitespace.contains(lastToken) && spacing.contains(currentToken)) {
                System.out.print(" ");
            }
            if (spacing.contains(lastToken) && !whitespace.contains(currentToken)) {
                System.out.print(" ");
            }
            System.out.print(token.getText());
            lastToken = currentToken;
        }
    }
}
