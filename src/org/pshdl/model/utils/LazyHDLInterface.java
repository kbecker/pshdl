package org.pshdl.model.utils;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.pshdl.model.HDLClass;
import org.pshdl.model.HDLExpression;
import org.pshdl.model.HDLInterface;
import org.pshdl.model.HDLVariableDeclaration;
import org.pshdl.model.IHDLObject;

import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;

public abstract class LazyHDLInterface extends HDLInterface {
    private ArrayList<HDLVariableDeclaration> cachedPorts;

    protected LazyHDLInterface(int id, IHDLObject container, String name, Iterable<HDLExpression> dim, boolean validate) {
        super(id, container, name, dim, null, validate);
    }

    public LazyHDLInterface() {
        super();
    }

    @Override
    public ArrayList<HDLVariableDeclaration> getPorts() {
        if (cachedPorts == null) {
            cachedPorts = getLazyPorts();
        }
        return cachedPorts;
    }

    protected abstract ArrayList<HDLVariableDeclaration> getLazyPorts();

    protected abstract LazyHDLInterface newInstance(int id, IHDLObject container, String name, Iterable<HDLExpression> dim, boolean validate);

    @Override
    public HDLInterface copy() {
        final HDLInterface newObject = new HDLInterface(id, null, name, dim, getPorts(), false);
        copyMetaData(this, newObject, false);
        return newObject;
    }

    @Override
    public HDLInterface copyFiltered(CopyFilter filter) {
        final String filteredname = filter.copyObject("name", this, name);
        final ArrayList<HDLExpression> filtereddim = filter.copyContainer("dim", this, dim);
        final ArrayList<HDLVariableDeclaration> filteredports = filter.copyContainer("ports", this, getPorts());
        return filter.postFilter((HDLInterface) this, new HDLInterface(id, null, filteredname, filtereddim, filteredports, false));
    }

    /**
     * Setter for the field {@link #getName()}.
     *
     * @param name
     *            sets the new name of this object. Can <b>not</b> be <code>null</code>.
     * @return a new instance of {@link HDLInterface} with the updated name field.
     */
    @Override
    @Nonnull
    public HDLInterface setName(@Nonnull String name) {
        name = validateName(name);
        final HDLInterface res = newInstance(id, container, name, dim, false);
        return res;
    }

    /**
     * Setter for the field {@link #getDim()}.
     *
     * @param dim
     *            sets the new dim of this object. Can be <code>null</code>.
     * @return a new instance of {@link HDLInterface} with the updated dim field.
     */
    @Override
    @Nonnull
    public HDLInterface setDim(@Nullable Iterable<HDLExpression> dim) {
        dim = validateDim(dim);
        final HDLInterface res = newInstance(id, container, name, dim, false);
        return res;
    }

    /**
     * Adds a new value to the field {@link #getDim()}.
     *
     * @param newDim
     *            the value that should be added to the field {@link #getDim()}
     * @return a new instance of {@link HDLInterface} with the updated dim field.
     */
    @Override
    @Nonnull
    public HDLInterface addDim(@Nullable HDLExpression newDim) {
        if (newDim == null) {
            throw new IllegalArgumentException("Element of dim can not be null!");
        }
        final ArrayList<HDLExpression> dim = (ArrayList<HDLExpression>) this.dim.clone();
        dim.add(newDim);
        final HDLInterface res = newInstance(id, container, name, dim, false);
        return res;
    }

    /**
     * Removes a value from the field {@link #getDim()}.
     *
     * @param newDim
     *            the value that should be removed from the field {@link #getDim()}
     * @return a new instance of {@link HDLInterface} with the updated dim field.
     */
    @Override
    @Nonnull
    public HDLInterface removeDim(@Nullable HDLExpression newDim) {
        if (newDim == null) {
            throw new IllegalArgumentException("Removed element of dim can not be null!");
        }
        final ArrayList<HDLExpression> dim = (ArrayList<HDLExpression>) this.dim.clone();
        dim.remove(newDim);
        final HDLInterface res = newInstance(id, container, name, dim, false);
        return res;
    }

    /**
     * Removes a value from the field {@link #getDim()}.
     *
     * @param idx
     *            the index of the value that should be removed from the field {@link #getDim()}
     * @return a new instance of {@link HDLInterface} with the updated dim field.
     */
    @Override
    @Nonnull
    public HDLInterface removeDim(int idx) {
        final ArrayList<HDLExpression> dim = (ArrayList<HDLExpression>) this.dim.clone();
        dim.remove(idx);
        final HDLInterface res = newInstance(id, container, name, dim, false);
        return res;
    }

    /**
     * Setter for the field {@link #getPorts()}.
     *
     * @param ports
     *            sets the new ports of this object. Can be <code>null</code>.
     * @return a new instance of {@link HDLInterface} with the updated ports field.
     */
    @Override
    @Nonnull
    public HDLInterface setPorts(@Nullable Iterable<HDLVariableDeclaration> ports) {
        ports = validatePorts(ports);
        final HDLInterface res = new HDLInterface(id, container, name, dim, ports, false);
        return res;
    }

    /**
     * Adds a new value to the field {@link #getPorts()}.
     *
     * @param newPorts
     *            the value that should be added to the field {@link #getPorts()}
     * @return a new instance of {@link HDLInterface} with the updated ports field.
     */
    @Override
    @Nonnull
    public HDLInterface addPorts(@Nullable HDLVariableDeclaration newPorts) {
        if (newPorts == null) {
            throw new IllegalArgumentException("Element of ports can not be null!");
        }
        final ArrayList<HDLVariableDeclaration> ports = getPorts();
        ports.add(newPorts);
        final HDLInterface res = new HDLInterface(id, container, name, dim, ports, false);
        return res;
    }

    /**
     * Removes a value from the field {@link #getPorts()}.
     *
     * @param newPorts
     *            the value that should be removed from the field {@link #getPorts()}
     * @return a new instance of {@link HDLInterface} with the updated ports field.
     */
    @Override
    @Nonnull
    public HDLInterface removePorts(@Nullable HDLVariableDeclaration newPorts) {
        if (newPorts == null) {
            throw new IllegalArgumentException("Removed element of ports can not be null!");
        }
        final ArrayList<HDLVariableDeclaration> ports = getPorts();
        ports.remove(newPorts);
        final HDLInterface res = new HDLInterface(id, container, name, dim, ports, false);
        return res;
    }

    /**
     * Removes a value from the field {@link #getPorts()}.
     *
     * @param idx
     *            the index of the value that should be removed from the field {@link #getPorts()}
     * @return a new instance of {@link HDLInterface} with the updated ports field.
     */
    @Override
    @Nonnull
    public HDLInterface removePorts(int idx) {
        final ArrayList<HDLVariableDeclaration> ports = getPorts();
        ports.remove(idx);
        final HDLInterface res = new HDLInterface(id, container, name, dim, ports, false);
        return res;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof LazyHDLInterface)) {
            return false;
        }
        if (!super.equals(obj)) {
            return false;
        }
        final LazyHDLInterface other = (LazyHDLInterface) obj;
        getPorts();
        other.getPorts();
        if (cachedPorts == null) {
            if (other.cachedPorts != null) {
                return false;
            }
        } else if (!cachedPorts.equals(other.cachedPorts)) {
            return false;
        }
        return true;
    }

    private Integer hashCache;

    @Override
    public int hashCode() {
        if (hashCache != null) {
            return hashCache;
        }
        int result = super.hashCode();
        final int prime = 31;
        getPorts();
        result = (prime * result) + ((cachedPorts == null) ? 0 : cachedPorts.hashCode());
        hashCache = result;
        return result;
    }

    @Override
    public String toConstructionString(String spacing) {
        final boolean first = true;
        final StringBuilder sb = new StringBuilder();
        sb.append('\n').append(spacing).append("new HDLInterface()");
        if (name != null) {
            sb.append(".setName(").append('"' + name + '"').append(")");
        }
        if (dim != null) {
            if (dim.size() > 0) {
                sb.append('\n').append(spacing);
                for (final HDLExpression o : dim) {
                    sb.append(".addDim(").append(o.toConstructionString(spacing + "\t\t"));
                    sb.append('\n').append(spacing).append(")");
                }
            }
        }
        if (ports != null) {
            if (ports.size() > 0) {
                sb.append('\n').append(spacing);
                for (final HDLVariableDeclaration o : getPorts()) {
                    sb.append(".addPorts(").append(o.toConstructionString(spacing + "\t\t"));
                    sb.append('\n').append(spacing).append(")");
                }
            }
        }
        return sb.toString();
    }

    @Override
    public void validateAllFields(IHDLObject expectedParent, boolean checkResolve) {
        super.validateAllFields(expectedParent, checkResolve);
        validatePorts(getPorts());
        if (getPorts() != null) {
            for (final HDLVariableDeclaration o : getPorts()) {
                o.validateAllFields(this, checkResolve);
            }
        }
    }

    @Override
    public EnumSet<HDLClass> getClassSet() {
        return EnumSet.of(HDLClass.HDLInterface, HDLClass.HDLType, HDLClass.HDLObject);
    }

    @Override
    public Iterator<IHDLObject> deepIterator() {
        return new Iterator<IHDLObject>() {

            private int pos = 0;
            private Iterator<? extends IHDLObject> current;

            @Override
            public boolean hasNext() {
                if ((current != null) && !current.hasNext()) {
                    current = null;
                }
                while (current == null) {
                    switch (pos++) {
                    case 0:
                        if ((dim != null) && (dim.size() != 0)) {
                            final List<Iterator<? extends IHDLObject>> iters = Lists.newArrayListWithCapacity(dim.size());
                            for (final HDLExpression o : dim) {
                                iters.add(Iterators.forArray(o));
                                iters.add(o.deepIterator());
                            }
                            current = Iterators.concat(iters.iterator());
                        }
                        break;
                    case 1:
                        getPorts();
                        if ((cachedPorts != null) && (cachedPorts.size() != 0)) {
                            final List<Iterator<? extends IHDLObject>> iters = Lists.newArrayListWithCapacity(cachedPorts.size());
                            for (final HDLVariableDeclaration o : cachedPorts) {
                                iters.add(Iterators.forArray(o));
                                iters.add(o.deepIterator());
                            }
                            current = Iterators.concat(iters.iterator());
                        }
                        break;
                    default:
                        return false;
                    }
                }
                return (current != null) && current.hasNext();
            }

            @Override
            public IHDLObject next() {
                return current.next();
            }

            @Override
            public void remove() {
                throw new IllegalArgumentException("Not supported");
            }

        };
    }

    @Override
    public Iterator<IHDLObject> iterator() {
        return new Iterator<IHDLObject>() {

            private int pos = 0;
            private Iterator<? extends IHDLObject> current;

            @Override
            public boolean hasNext() {
                if ((current != null) && !current.hasNext()) {
                    current = null;
                }
                while (current == null) {
                    switch (pos++) {
                    case 0:
                        if ((dim != null) && (dim.size() != 0)) {
                            current = dim.iterator();
                        }
                        break;
                    case 1:
                        getPorts();
                        if ((cachedPorts != null) && (cachedPorts.size() != 0)) {
                            current = cachedPorts.iterator();
                        }
                        break;
                    default:
                        return false;
                    }
                }
                return (current != null) && current.hasNext();
            }

            @Override
            public IHDLObject next() {
                return current.next();
            }

            @Override
            public void remove() {
                throw new IllegalArgumentException("Not supported");
            }

        };
    }
}
