/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *     
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for 
 *     reasonable and customary use in describing the origin of the Work.
 * 
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.model.simulation.codegenerator

import com.google.common.base.Splitter
import com.google.common.collect.Lists
import com.google.common.collect.Maps
import com.google.common.io.ByteStreams
import com.google.common.io.Files
import java.io.File
import java.io.FileOutputStream
import java.math.BigInteger
import java.nio.charset.StandardCharsets
import java.util.EnumSet
import java.util.LinkedHashMap
import java.util.LinkedHashSet
import java.util.List
import java.util.Map
import java.util.Set
import org.apache.commons.cli.CommandLine
import org.apache.commons.cli.Options
import org.pshdl.interpreter.ExecutableModel
import org.pshdl.interpreter.Frame
import org.pshdl.interpreter.Frame.FastInstruction
import org.pshdl.interpreter.FunctionInformation
import org.pshdl.interpreter.IHDLInterpreterFactory
import org.pshdl.interpreter.InternalInformation
import org.pshdl.interpreter.NativeRunner
import org.pshdl.interpreter.ParameterInformation
import org.pshdl.interpreter.VariableInformation
import org.pshdl.interpreter.VariableInformation.Type
import org.pshdl.interpreter.utils.Instruction
import org.pshdl.model.simulation.ITypeOuptutProvider
import org.pshdl.model.simulation.SimulationTransformationExtension
import org.pshdl.model.types.builtIn.busses.memorymodel.BusAccess
import org.pshdl.model.types.builtIn.busses.memorymodel.Definition
import org.pshdl.model.types.builtIn.busses.memorymodel.MemoryModel
import org.pshdl.model.types.builtIn.busses.memorymodel.Row
import org.pshdl.model.types.builtIn.busses.memorymodel.Unit
import org.pshdl.model.types.builtIn.busses.memorymodel.v4.MemoryModelAST
import org.pshdl.model.utils.PSAbstractCompiler.CompileResult
import org.pshdl.model.utils.services.AuxiliaryContent
import org.pshdl.model.utils.services.IOutputProvider.MultiOption
import org.pshdl.model.validation.Problem
import org.pshdl.interpreter.NativeRunner.IRunListener
import org.pshdl.model.evaluation.HDLEvaluationContext
import org.pshdl.model.IHDLObject
import org.pshdl.model.types.builtIn.HDLBuiltInAnnotationProvider.HDLBuiltInAnnotations
import org.pshdl.model.types.builtIn.busses.BusGenerator
import org.pshdl.model.types.builtIn.busses.memorymodel.Constant
import java.util.ArrayList
import org.pshdl.model.types.builtIn.busses.memorymodel.Constant.ConstantType
import org.pshdl.model.types.builtIn.busses.memorymodel.RowOrBlockRam
import org.pshdl.model.types.builtIn.busses.memorymodel.BlockRam
import org.pshdl.model.types.builtIn.busses.memorymodel.NamedElement
import java.util.Collections
import java.lang.invoke.MethodHandles.Lookup

class CCodeGeneratorParameter extends CommonCodeGeneratorParameter {
	new(ExecutableModel em, HDLEvaluationContext context) {
		super(em, 64, context)
	}
	@Option(description = "Generate C++ simulation code", optionName = "cpp", hasArg = false)
	public boolean cpp
	@Option(description="The name of the c++ class. If not specified, the name of the module will be used", optionName="name", hasArg=true)
	public String unitName;
}

class CCodeGenerator extends CommonCodeGenerator implements ITypeOuptutProvider {

	private CommonCompilerExtension cce
	private boolean hasPow = false
	private boolean cpp = false;
	private String unitName = "PSHDLModule"
	public static String COMPILER = "/usr/bin/clang"
	public List<CharSequence> methods=new ArrayList

	new() {
	}

	new(CCodeGeneratorParameter parameter) {
		super(parameter)
		this.cpp = parameter.cpp;
		if (parameter.unitName!==null) {
			unitName=parameter.unitName;
		} else {
			val moduleName=parameter.em.moduleName
			unitName=moduleName.substring(moduleName.lastIndexOf('.')+1);
		}
 		this.cce = new CommonCompilerExtension(em, 64)
	}

	def IHDLInterpreterFactory<NativeRunner> createInterpreter(File tempDir, IRunListener listener, HDLEvaluationContext context) {
		val String ext=if (cpp) ".cpp" else ".c"
		val File testCFile = new File(tempDir, "test"+ext)
		Files.write(generateMainCode, testCFile, StandardCharsets.UTF_8)
		val File testRunner = new File(tempDir, "runner"+ext)
		copyFile("/org/pshdl/model/simulation/includes/runner"+ext, testRunner)
//		val File testGenericLib = new File(tempDir, "pshdl_generic_sim.c")
//		copyFile("/org/pshdl/model/simulation/includes/pshdl_generic_sim.c", testGenericLib)
		if (cpp) {
			val File testGenericLibHeader = new File(tempDir, "pshdl_generic_sim.hpp")
			copyFile("/org/pshdl/model/simulation/includes/pshdl_generic_sim.hpp", testGenericLibHeader)
		} else {
			val File testGenericLibHeader = new File(tempDir, "pshdl_generic_sim.h")
			copyFile("/org/pshdl/model/simulation/includes/pshdl_generic_sim.h", testGenericLibHeader)
		}
		val File executable = new File(tempDir, "testExec")
		writeAuxiliaryContents(tempDir, context)
		var command=COMPILER
		if (cpp)
			command+="++"
		val List<String> args=newArrayList(command, "-I", tempDir.absolutePath, "-O3",
			testCFile.absolutePath, testRunner.absolutePath, "-o", executable.absolutePath)
		if (cpp)
			args.add("-std=c++11")
		val ProcessBuilder builder = new ProcessBuilder(args)
		val Process process = builder.directory(tempDir).inheritIO().start()
		process.waitFor()
		val exitValue = process.exitValue()
		if (exitValue != 0) {		
			throw new RuntimeException("Process did not terminate with 0, was "+exitValue)
		}
		return new IHDLInterpreterFactory<NativeRunner>() {
			override newInstance() {
				val ProcessBuilder execBuilder = new ProcessBuilder(executable.getAbsolutePath())
				val Process testExec = execBuilder.directory(tempDir).redirectErrorStream(true).start()
				return new NativeRunner(testExec.getInputStream(), testExec.getOutputStream(), em, testExec, 5,
					executable.getAbsolutePath(), listener)
			}
		}
	}
	
	def copyFile(String fileToCopy, File testRunner) {
		val runnerStream = typeof(CCodeGenerator).getResourceAsStream(fileToCopy)
		val fos = new FileOutputStream(testRunner)
		try {
			ByteStreams.copy(runnerStream, fos)
		} finally {
			runnerStream.close
			fos.close
		}
	}

	override protected applyRegUpdates() '''updateRegs();'''

	override protected assignArrayInit(VariableInformation hvar, BigInteger initValue,
		EnumSet<CommonCodeGenerator.Attributes> attributes) '''«fieldName(hvar, attributes)»[«hvar.arraySize»];'''

	override protected arrayInit(VariableInformation varInfo, BigInteger zero,
		EnumSet<CommonCodeGenerator.Attributes> attributes) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

	override protected callStage(int stage, boolean constant) '''«stageMethodName(stage, constant)»();
		'''

	override protected checkRegupdates() '''regUpdatePos!=0'''

	override protected clearRegUpdates() '''regUpdatePos=0;'''

	override protected fieldType(VariableInformation varInfo, EnumSet<CommonCodeGenerator.Attributes> attributes) {
		if (varInfo.type==Type.STRING)
			return "char*"
		if (isBoolean(varInfo, attributes))
			return "bool"
		return "uint64_t"
	}

	override protected justDeclare(VariableInformation varInfo, EnumSet<CommonCodeGenerator.Attributes> attributes) '''«fieldName(
		varInfo, attributes)»«IF varInfo.array»[«varInfo.arraySize»]«ENDIF»;'''

	override protected footer() '''
		«helperMethods»
	'''

	override protected postFieldDeclarations() ''''''
	
	def CPPUnit() {
		if (cpp)
			return unitName+"::"
		return ""
	}
	def CPPMethod() {
		if (cpp)
			return unitName+"::"
		return "pshdl_sim_"
	}

	def CPPDut() {
		if (cpp)
			return "dut->"
		return "pshdl_sim_"
	}
	
	override protected functionFooter(Frame frame) '''}
		'''

	override protected functionHeader(Frame frame) {
		val name=frame.frameName
		methods.add('''void «name»();''')
		val res='''«IF !cpp»static «ENDIF»void «CPPUnit»«name»() {
		'''
		return res
	}

	override protected header() '''
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
«IF cpp»
#include "pshdl_generic_sim.hpp"
#include "«headerName».hpp"
«ELSE»
#include "pshdl_generic_sim.h"
#include "«headerName».h"
«ENDIF»
«generateInlineMethods»

«IF hasClock && !cpp»
	/// Don't use this
	typedef struct regUpdate {
		int internal;
		int offset;
		uint64_t fillValue;
	} regUpdate_t;
	
	static regUpdate_t regUpdates[«maxRegUpdates»];
	static int regUpdatePos=0;
«ENDIF»

«IF! cpp»
static void updateRegs();
static bool skipEdge(uint64_t local);
static uint32_t hash(char* str);
postFunc_t postFunc;
«ENDIF»

'''
	def generateInlineMethods() {
		val StringBuilder sb = new StringBuilder
		for (FunctionInformation fi : em.functions) {
				sb.
					append('''extern «fi.returnType.toC» «fi.signature»(«FOR ParameterInformation pi : fi.parameter SEPARATOR ', '»«pi.toC» p«pi.name.toFirstUpper»«ENDFOR»);''')
		}
		return sb
	}
	
	def toC(ParameterInformation information) {
		if (information === null)
			return "void"
		if (information.type === ParameterInformation.Type.PARAM_BOOL)
			return "bool"
		if (information.type === ParameterInformation.Type.PARAM_STRING)
			return "const char*"
		return "uint64_t"
	}
	
	def protected copyRegs() '''
		«IF !cpp»static«ENDIF» void «CPPUnit»updateRegs() {
			int i;
			for (i=0;i<regUpdatePos; i++) {
				regUpdate_t reg=regUpdates[i];
				switch (reg.internal) {
					«updateRegCases»
				}
			}
		}
	'''

	def static int hash(String str) {
		var int hash = -2128831035;
		val byte[] bytes = str.getBytes(StandardCharsets.ISO_8859_1);
		for (byte b : bytes) {
			hash = hash.bitwiseXor(b);
			hash = hash * 16777619;
		}
		return hash;
	}

	override protected runMethodsFooter(boolean constant) '''	if (postFunc != 0) postFunc();
	}
		'''

	override protected runMethodsHeader(boolean constant) '''void «CPPMethod»«IF !constant»run«ELSE»initConstants«ENDIF»() {
		'''

	override protected scheduleShadowReg(InternalInformation outputInternal, CharSequence last, CharSequence cpyName,
		CharSequence offset, boolean force, CharSequence fillValue) '''
		«IF !force»if («cpyName»!=«last»)
		«indent()»	«ENDIF»{
		«indent()»		static regUpdate_t reg;
		«indent()»		reg.internal=«outputInternal.regIdx»;
		«indent()»		reg.offset=(int)(«offset»);
		«indent()»		reg.fillValue=«fillValue»;
		«indent()»		regUpdates[regUpdatePos++]=reg;
		«indent()»	}
	'''

	def regIdx(InternalInformation information) {
		regIdx.get(information.info.name)
	}

	override protected stageMethodsFooter(int stage, int totalStageCosts, boolean constant) '''}
		'''

	override protected stageMethodsHeader(int stage, int totalStageCosts, boolean constant) {
		val stageName=stageMethodName(stage, constant)
		methods.add('''void «stageName»();''')
		val res='''«IF !cpp»static void «ELSE»void «CPPMethod»«ENDIF»«stageName»(){
		'''
		return res
	}

	override protected getCast(int targetSizeWithType) {
		if (isSignedType(targetSizeWithType))
			return "(int64_t)"
		return ""
	}

	override protected twoOp(FastInstruction fi, String op, int targetSizeWithType, int pos, int leftOperand,
		int rightOperand, EnumSet<CommonCodeGenerator.Attributes> attributes, boolean doMask) {
			val type=typeFromTargetSize(targetSizeWithType)
		switch (fi.inst) {
			case Instruction.sra:
				return assignTempVar(type, targetSizeWithType, pos, attributes,
					'''((int64_t)«getTempName(leftOperand, NONE)») >> «getTempName(rightOperand, NONE)»''', true)
			case Instruction.srl:
				return assignTempVar(type, targetSizeWithType, pos, attributes,
					'''«getTempName(leftOperand, NONE)» >> «getTempName(rightOperand, NONE)»''', true)
			case Instruction.less:
				return assignTempVar(type, targetSizeWithType, pos, attributes,
					'''(int64_t)«getTempName(leftOperand, NONE)» < (int64_t)«getTempName(rightOperand, NONE)»''', true)
			case Instruction.less_eq:
				return assignTempVar(type, targetSizeWithType, pos, attributes,
					'''(int64_t)«getTempName(leftOperand, NONE)» <= (int64_t)«getTempName(rightOperand, NONE)»''', true)
			case Instruction.greater:
				return assignTempVar(type, targetSizeWithType, pos, attributes,
					'''(int64_t)«getTempName(leftOperand, NONE)» > (int64_t)«getTempName(rightOperand, NONE)»''', true)
			case Instruction.greater_eq:
				return assignTempVar(type, targetSizeWithType, pos, attributes,
					'''(int64_t)«getTempName(leftOperand, NONE)» >= (int64_t)«getTempName(rightOperand, NONE)»''', true)
			default: {
			}
		}
		super.twoOp(fi, op, targetSizeWithType, pos, leftOperand, rightOperand, attributes, doMask)
	}

	override protected copyArray(VariableInformation varInfo) '''memcpy(«varInfo.idName(true,
		EnumSet.of(CommonCodeGenerator.Attributes.isPrev))», «varInfo.idName(true, NONE)», «varInfo.arraySize»);
		'''

	override protected preField(VariableInformation x, EnumSet<CommonCodeGenerator.Attributes> attributes) '''«IF !attributes.
		contains(CommonCodeGenerator.Attributes.isPublic) && !cpp»static«ENDIF» '''

	def protected helperMethods() '''
		«IF hasClock»
		«IF !cpp»static«ENDIF» bool «CPPUnit»skipEdge(uint64_t local) {
			uint64_t dc = local >> 16l;
			// Register was updated in previous delta cylce, that is ok
			if (dc < deltaCycle)
				return false;
			// Register was updated in this delta cycle but it is the same eps,
			// that is ok as well
			if ((dc == deltaCycle) && ((local & 0xFFFF) == epsCycle))
				return false;
			// Don't update
			return true;
		}
		«copyRegs»
		«ENDIF»
		void «CPPMethod»setInputArray(uint32_t idx, uint64_t value, uint32_t offset) {
			switch (idx) {
				«setInputCases("value", null, EnumSet.of(Attributes.useArrayOffset))»
			}
		}
		void «CPPMethod»setInput(uint32_t idx, uint64_t value) {
			«IF !cpp»pshdl_sim_«ENDIF»setInputArray(idx, value, 0);
		}
		
		«IF cpp»std::string«ELSE»char*«ENDIF» «CPPMethod»getName(uint32_t idx) {
			switch (idx) {
				«FOR v : em.variables»
					case «v.getVarIdx(false)»: return "«v.name»";
				«ENDFOR»
			}
			return 0;
		}
		
		«IF cpp»std::string«ELSE»static char*«ENDIF» jsonDesc="«cce.JSONDescription»";
		«IF cpp»std::string«ELSE»char*«ENDIF» «CPPMethod»getJsonDesc(){
			return jsonDesc;
		}
		
		uint64_t «CPPMethod»getDeltaCycle(){
			return deltaCycle;
		}
		
		uint32_t «CPPMethod»getVarCount(){
			return «varIdx.size»;
		}
		
		void «CPPMethod»setDisableEdges(bool enable){
			«IF hasClock»
				«DISABLE_EDGES.name»=enable;
			«ENDIF»
		}
		
		void «CPPMethod»setDisableRegOutputlogic(bool enable){
			«IF hasClock»
				«DISABLE_REG_OUTPUTLOGIC.name»=enable;
			«ENDIF»
		}
		
		«IF !cpp»static «ENDIF»uint32_t «CPPUnit»hash(char* str){
			size_t len=strlen(str);
			uint32_t hash = 2166136261;
			int i;
			for (i=0;i<len;i++){
			   	hash = hash ^ str[i];
			   	hash = hash * 16777619;
			   }
			return hash;
		}
		
		int «CPPMethod»getIndex(char* name) {
			uint32_t hashName=hash(name);
			switch (hashName) {
				«FOR Map.Entry<Integer, List<VariableInformation>> e : em.variables.hashed.entrySet»
					case «constant32Bit(e.key)»:
						«FOR VariableInformation vi : e.value» 
							if (strcmp(name, "«vi.name»") == 0)
								return «vi.getVarIdx(purgeAliases)»;
						«ENDFOR»
						return -1; //so close...
				«ENDFOR»
			default:
				return -1;
			}
		}
		
		
		uint64_t «CPPMethod»getOutputArray(uint32_t idx, uint32_t offset) {
			switch (idx) {
				«getOutputCases(null, EnumSet.of(Attributes.useArrayOffset))»
			}
			return 0;
		}
		uint64_t «CPPMethod»getOutput(uint32_t idx) {
			return «IF !cpp»pshdl_sim_«ENDIF»getOutputArray(idx, 0);
		}
		
		«IF hasPow»
			static uint64_t «CPPMethod»pow(uint64_t a, uint64_t n){
			    uint64_t result = 1;
			    uint64_t p = a;
			    while (n > 0){
			        if ((n % 2) != 0)
			            result = result * p;
			        p = p * p;
			        n = n / 2;
			    }
			    return result;
			}
		«ENDIF»
	'''

	override def boolean declareFields(){
		return !cpp
	}

	def Map<Integer, List<VariableInformation>> getHashed(Iterable<VariableInformation> informations) {
		val Map<Integer, List<VariableInformation>> res = Maps.newLinkedHashMap()
		for (VariableInformation vi : em.variables) {
			val hashVal = hash(vi.name)
			val list = res.get(hashVal)
			if (list === null) {
				res.put(hashVal, Lists.newArrayList(vi))
			} else {
				list.add(vi)
			}
		}
		return res
	}

	override protected barrier() '''
		}
		#pragma omp section
		{
	'''

	override protected barrierBegin(int stage, int totalStageCosts, boolean createConstant) {
		indent += 2
		'''
			#pragma omp parallel sections
			«indent()»{
			«indent()»#pragma omp section
			«indent()»{
		'''
	}

	override protected barrierEnd(int stage, int totalStageCosts, boolean createConstant) {
		indent -= 2
		'''
				}
			«indent()»}
		'''
	}

	override getAuxiliaryContent(HDLEvaluationContext context) {
		if (cpp){
			val generic_hStream = typeof(CCodeGenerator).getResourceAsStream(
				"/org/pshdl/model/simulation/includes/pshdl_generic_sim.hpp")
			try {
				val generic_h = new AuxiliaryContent("pshdl_generic_sim.hpp", generic_hStream, true)
				val specific_h = new AuxiliaryContent(headerName() + ".hpp", specificHeader.toString)
				val privateMethods_h = new AuxiliaryContent(headerName()+"_private.hpp", methods.join("\n"))
				val res = Lists.newArrayList(generic_h, specific_h, privateMethods_h)
				val simEncapsulation = generateSimEncapsuation(context, null)
				if (simEncapsulation !== null) {
					res.add(new AuxiliaryContent("simEncapsulation.cpp", simEncapsulation))
				}
				return res
			} finally {
				generic_hStream.close
			}
		} else {
			val generic_hStream = typeof(CCodeGenerator).getResourceAsStream(
				"/org/pshdl/model/simulation/includes/pshdl_generic_sim.h")
			try {
				val generic_h = new AuxiliaryContent("pshdl_generic_sim.h", generic_hStream, true)
	
				val specific_h = new AuxiliaryContent(headerName() + ".h", specificHeader.toString)
				val res = Lists.newArrayList(generic_h, specific_h)
				val simEncapsulation = generateSimEncapsuation(context, null)
				if (simEncapsulation !== null) {
					res.add(new AuxiliaryContent("simEncapsulation.c", simEncapsulation))
				}
				return res
			} finally {
				generic_hStream.close
			}
		}
	}

	def protected headerName() {
		"pshdl_" + unitName + "_sim"
	}

	def protected getSpecificHeader() '''/**
 * @file
 * @brief Provides access to all fields and their index.
 */

#ifndef _«headerName»_h_
#define _«headerName»_h_
«IF !cpp»
#ifdef __cplusplus
extern "C" {
#endif
#include "pshdl_generic_sim.h"
«ELSE»
#include "pshdl_generic_sim.hpp"
«ENDIF»

«FOR VariableInformation vi : em.variables»
	///Use this index define to access <tt> «vi.name.replaceAll("\\@", "\\\\@")» </tt> via getOutput/setInput methods
	#define «vi.defineName» «vi.getVarIdx(purgeAliases)»
«ENDFOR»
«IF cpp»
class «unitName» : public PSHDLSim {
private:
  «fieldDeclarations(true, false, true)»
#include "«headerName»_private.hpp"
«IF cpp»
	/// Don't use this
	typedef struct regUpdate {
		int internal;
		int offset;
		uint64_t fillValue;
	} regUpdate_t;
	
	regUpdate_t regUpdates[«maxRegUpdates»];
	int regUpdatePos=0;
«ENDIF»

protected:
  uint32_t hash(char* str);
  bool skipEdge(uint64_t local);
  void updateRegs();

public:
  uint64_t getOutput(uint32_t idx);
  void     setInput (uint32_t idx, uint64_t value);

  uint64_t getOutputArray(uint32_t idx, uint32_t offset);
  void     setInputArray (uint32_t idx, uint64_t value, uint32_t offset);
 
  void     setDisableRegOutputlogic(bool enable);
  void     setDisableEdges(bool enable);
 
  void     run(void);
  void     initConstants(void);
 
  int      getIndex(char* name);
  std::string getName(uint32_t idx);
  std::string getJsonDesc(void);
  uint64_t getDeltaCycle(void);
  uint32_t getVarCount(void);
  
  postFunc_t postFunc;
  «fieldDeclarations(false, true, true)»

};
«ELSE»
«fieldDeclarations(false, false).toString.split("\n").map["extern" + it].join("\n")»
#ifdef __cplusplus
}
#endif
«ENDIF»
#endif
	'''

	def String generateSimEncapsuation(HDLEvaluationContext context, IHDLObject container) {
		val Unit unit = getUnit(em)
		if (unit === null)
			return null
		return generateSimEncapsuation(unit, MemoryModel.buildRows(unit, context, container))
	}

	def getUnit(ExecutableModel model) {
		var Unit unit
		val annoSplitter = Splitter.on(SimulationTransformationExtension.ANNO_VALUE_SEP);
		var String prefix
		val busDescription=BusGenerator.busDescription.name.substring(1)
		val busPrefix=BusGenerator.busPrefix.name.substring(1)
		if (em.annotations !== null) {
			for (a : em.annotations) {
				if (a.startsWith(busDescription)) {
					val value = annoSplitter.limit(2).split(a).last
					unit = MemoryModelAST.parseUnit(value, new LinkedHashSet, 0)
				}
				if (a.startsWith(busPrefix)){
					prefix=annoSplitter.limit(2).split(a).last
				}
			}
		}
		if (unit!==null)
			unit.prefix=prefix
		return unit
	}

	extension BusAccess ba = new BusAccess

	private def generateSimEncapsuation(Unit unit, Iterable<RowOrBlockRam> rows) {
		var prefix=unit.prefix
		if (prefix!="")
			prefix+="_"
		val Set<String> varNames = new LinkedHashSet
		rows.forEach[it.allDefs.filter[it.type !== BlockRam.Type.UNUSED].forEach[varNames.add(it.getName)]]
		var res = '''
/**
 * @file
 * @brief  Provides methods for simulating accessing to the memory registers
 *
 * This file is a substitue for the BusAccess.c file that is used to access real memory.
 * For each type of row there are methods for setting/getting the values
 * either directly, or as a struct. A memory map overview has been
 * generated into BusMap.html.
 */

#include <stdint.h>
#include <stdbool.h>
#include "«unit.prefix»BusAccess.h"
#include "BusStdDefinitions.h"
«IF cpp»
#include "«headerName».hpp"
extern «unitName» *dut;
«ELSE»
#include "«headerName».h"
«ENDIF»

/**
 * This method provides a null implementation of the warning functionality. You
 * can use it to provide your own error handling, or you can use the implementation
 * provided in BusPrint.h
 */
static void «prefix»defaultWarn(warningType_t t, uint32_t value, const char *def, const char *row, const char *msg){
}

warnFunc_p «prefix»warn=«prefix»defaultWarn;

/**
 * This methods allows the user to set a custom warning function. Usually this is used
 * in conjunction with the implementation provided in BusPrint.h.
 *
 * @param warnFunction the new function to use for error reporting
 *
 * Example Usage:
 * @code
 *    #include "BusPrint.h"
 *    setWarn(defaultPrintfWarn);
 * @endcode
 */
void «prefix»setWarn(warnFunc_p warnFunction){
    «prefix»warn=warnFunction;
}

///The index of the Clock that is toggled for each setting
#define busclk_idx «busIndex»

int «prefix»getOffset(«prefix»rows_t row, unsigned int index){
	«val x=rows.toIdxMap»
	switch(row) {
	«FOR entry: x.entrySet»
		case «prefix»row_«entry.key»:
			if (index >= «entry.value.size») return -1;
			«IF entry.value.size==1»return «entry.value.get(0)»;
			«ELSE»
			switch (index) {
				«FOR i:0..<entry.value.size»
				case «i»: return «entry.value.get(i)»;
				«ENDFOR»
			}
			«ENDIF»
	«ENDFOR»
	}
}

uint32_t «prefix»writeMemory(uint32_t *base, unsigned int offset, uint32_t newVal){
	if (offset > «unit.lastBase»)
		return 0;
	«CPPDut»setInput(«varIdx.get(em.moduleName + ".PADDR")», offset<<2); //Set PADDR
	«CPPDut»setInput(«varIdx.get(em.moduleName + ".PWRITE")», 1);   //Set PWRITE
	«CPPDut»setInput(«varIdx.get(em.moduleName + ".PSEL")», 1);     //Set PSEL
	«CPPDut»setInput(«varIdx.get(em.moduleName + ".PENABLE")», 0);  //Set PENABLE
	«CPPDut»setInput(«varIdx.get(em.moduleName + ".PWDATA")», newVal);   //Set PWDATA
	«CPPDut»run();
	«CPPDut»setInput(«varIdx.get(em.moduleName + ".PENABLE")», 1);  //Set PENABLE
	«CPPDut»run();
	«CPPDut»setInput(«varIdx.get(em.moduleName + ".PENABLE")», 0);  //Set PENABLE
	«CPPDut»setInput(«varIdx.get(em.moduleName + ".PSEL")», 0);     //Set PSEL
	«CPPDut»setInput(«varIdx.get(em.moduleName + ".PWRITE")», 0);   //Set PWRITE
	«CPPDut»run();
	return 1;
}

uint32_t «prefix»readMemory(uint32_t *base, unsigned int offset, uint32_t *readVal){
	if (offset > «unit.lastBase»)
		return 0;
	«CPPDut»setInput(«varIdx.get(em.moduleName + ".PADDR")», offset<<2); //Set PADDR
	«CPPDut»setInput(«varIdx.get(em.moduleName + ".PWRITE")», 0);   //Set PWRITE
	«CPPDut»setInput(«varIdx.get(em.moduleName + ".PSEL")», 1);     //Set PSEL
	«CPPDut»setInput(«varIdx.get(em.moduleName + ".PENABLE")», 0);  //Set PENABLE
	«CPPDut»run();
	«CPPDut»setInput(«varIdx.get(em.moduleName + ".PENABLE")», 1);  //Set PENABLE
	uint64_t isReady=0;
	do {
		«CPPDut»run();
		isReady=«CPPDut»getOutput(«varIdx.get(em.moduleName + ".PREADY")»); //Checking for PREADY becoming 1
	} while (isReady==0);
	*readVal=(uint32_t)«CPPDut»getOutput(«varIdx.get(em.moduleName + ".PRDATA")»);
	«CPPDut»setInput(«varIdx.get(em.moduleName + ".PENABLE")», 0);  //Set PENABLE
	«CPPDut»setInput(«varIdx.get(em.moduleName + ".PSEL")», 0);     //Set PSEL
	«CPPDut»setInput(«varIdx.get(em.moduleName + ".PWRITE")», 0);   //Set PWRITE
	«CPPDut»run();
	return 1;
}

«IF rows.map[it.definitions.head].findFirst[it instanceof Constant && (it as Constant).constType === ConstantType.checksum] !== null»
/**
 * Retrievs the checkSumField and validates it against the known value «prefix.prefix»checkSumValue 0x«unit.checkSum.hex32».
 * A mismatch between the memory mapped value and the known value may indicate that the drivers
 * do not match the firmware that is on the FPGA!
 *
 * @param base a (volatile) pointer to the memory offset at which the IP core can be found in memory.
 * 
 * @retval true the memory mapped value matches the known value
 * @retval false the memory mapped differs from the known value
 *
 */
bool «prefix»validateCheckSumMatch(uint32_t *base){
	uint32_t chkSum;
	«prefix»readMemory(base, «rows.findCheckSumIdx», &chkSum);
	return chkSum == 0x«unit.checkSum.hex32»;
}
«ENDIF»
'''
		val checkedRows = new LinkedHashSet<String>()
		val rowCounts = new LinkedHashMap<String, Integer>()
		for (row : rows) {
			val idx = rowCounts.get(row.name);
			if (idx === null)
				rowCounts.put(row.name, 1)
			else
				rowCounts.put(row.name, idx + 1)
		}
		for (row : rows) {
			if (!checkedRows.contains(row.name)) {
				if (row instanceof Row) {
					if (row.hasWriteDefs)
						res = res + row.simSetter(rows, rowCounts.get(row.name), prefix, rowCounts.get(row.name)>1)
					res = res + row.simGetter(rows, rowCounts.get(row.name), prefix, rowCounts.get(row.name)>1)
					checkedRows.add(row.name)
				}
			}
		}
		return res
	}
	
	def List<NamedElement> getDefinitions(RowOrBlockRam ram){
		if (ram instanceof Row)
			return ram.definitions
		return Collections.emptyList
	}
	
	def Map<String, List<Integer>> toIdxMap(Iterable<RowOrBlockRam> rows){
		val res=new LinkedHashMap<String, List<Integer>>()
		for (row : rows) {
			var list=res.get(row.name)
			if (list===null)
				list=new ArrayList()
			list.add(row.address)
			res.put(row.name, list)
		}
		return res
	}
	def protected int getBusIndex() {
		val pclk = varIdx.get(em.moduleName + ".PCLK")
		if (pclk === null)
			return varIdx.get(em.moduleName + ".Bus2IP_Clk")
		return pclk
	}

	def protected getDefineName(VariableInformation vi) '''PSHDL_SIM_«vi.idName(true, NONE).toString.toUpperCase»'''

	def protected getDefineNameString(String s) '''PSHDL_SIM_«(em.moduleName + "." + s).idName(true, NONE).toString.
		toUpperCase»'''

	def protected simGetter(Row row, Iterable<RowOrBlockRam> rows, int rowCount, String prefix, boolean isArray) '''
/**
 * Directly retrieve the fields of row «row.name».
 *
 * @param base a (volatile) pointer to the memory offset at which the IP core can be found in memory. For simulation this parameter is ignored.
 *«IF isArray» @param index the row that you want to access. Valid values are 0..«rowCount -
		1»«ENDIF»
 «FOR Definition d : row.allDefs»
 * @param «d.name» the value of «d.name» will be written into the memory of this pointer.
 «ENDFOR»
 *
 * @retval 1  Successfully retrieved the values
 * @retval 0  Something went wrong (invalid index for example)
 *
 */
int «prefix»get«row.name.toFirstUpper»Direct(uint32_t *base«IF isArray», uint32_t index«ENDIF»«FOR Definition definition : row.allDefs»«getParameter(
		row, definition, true)»«ENDFOR»){
	«FOR Definition d : row.allDefs»
		«IF d instanceof Constant»
			*«row.getVarName(d)»=«String.format("%#08x",MemoryModel.dimToInt(context, d.value, null))»;
		«ELSE»
			«IF isArray»
			*«row.getVarName(d)»=(«d.busType»)«CPPDut»getOutputArray(«d.getName(row).getDefineNameString», index);
			«ELSE»
			*«row.getVarName(d)»=(«d.busType»)«CPPDut»getOutput(«d.getName(row).getDefineNameString»);
			«ENDIF»
		«ENDIF»
	«ENDFOR»
	#ifndef PSHDL_SIM_NO_BUSCLK_TOGGLE
	if (!«IF cpp»dut->«ENDIF»«DISABLE_EDGES.name») {
		«CPPDut»setInput(busclk_idx, 0);
		«CPPDut»run();
	}
	«CPPDut»setInput(busclk_idx, 1);
	#ifdef PSHDL_SIM_FULL_BUS_WRITE
	int addr=«prefix»getOffset(«prefix»row_«row.name», «IF isArray»index«ELSE»0«ENDIF»);
	uint32_t dummy;
	«prefix»readMemory(base, addr, &dummy);
	#else
	«CPPDut»run();
	#endif
	#endif	
	return 1;
}

/**
 * Retrieve the fields of row «row.name» into the struct.
 *
 * @param base a (volatile) pointer to the memory offset at which the IP core can be found in memory. For simulation this parameter is ignored.
«IF isArray» * @param index the row that you want to access. Valid values are 0..«rowCount -
		1»«ENDIF»
 * @param result the values of this row will be written into the struct
 *
 * @retval 1  Successfully retrieved the values
 * @retval 0  Something went wrong (invalid index for example)
 *
 */
int «prefix»get«row.name.toFirstUpper»(uint32_t *base«IF isArray», uint32_t index«ENDIF», «prefix»«row.name»_t *result){
	return «prefix»get«row.name.toFirstUpper»Direct(base«IF isArray», index«ENDIF»«FOR Definition d : row.allDefs», &result->«row.getVarNameIndex(d)»«ENDFOR»);
}
'''
	def generateAddressSwitch(Row row, Iterable<Row> rows, boolean isArray) {
		var idx = 0
		var rIdx = 0
		var res = '''«IF isArray»switch (index) {«ENDIF»
			'''
		for (Row r : rows) {
			if (r.name.equals(row.name)) {
				if (isArray) {
					res = res + '''
						case «rIdx»: addr=«idx»;break;
					'''
					rIdx = rIdx + 1
				} else {
					res = res + '''
						addr=«idx»;
					'''
				}
			}
			idx = idx + 1
		}
		res = res + '''«IF isArray»default: return 0;
		}«ENDIF»
			'''
		return res
	}
	def protected simSetter(Row row, Iterable<RowOrBlockRam> rows, int rowCount, String prefix, boolean isArray) '''
/**
 * Updates the values in memory from the struct. This also advances the simulation by one clock cycle, 
 * unless PSHDL_SIM_NO_BUSCLK_TOGGLE is defined.
 *
 * @param base a (volatile) pointer to the memory offset at which the IP core can be found in memory. For simulation this parameter is ignored.
 *«IF isArray» @param index the row that you want to access. Valid values are 0..«rowCount - 1»«ENDIF»
 «FOR Definition d : row.writeDefs»
 * @param «d.name» the value of «d.name» will be written into the register. «explain(d)»
 «ENDFOR»
 *
 * @retval 1  Successfully updated the values
 * @retval 0  Something went wrong (invalid index or value exceeds its range for example)
 *
 */
int «prefix»set«row.name.toFirstUpper»Direct(uint32_t *base«IF isArray», uint32_t index«ENDIF»«FOR Definition definition : row.writeDefs»«getParameter(
		row, definition, false)»«ENDFOR»){
	«IF isArray»
	if (index>«rowCount - 1»)
		return 0;
	«ENDIF»
	«FOR Definition ne : row.writeDefs»
		«row.generateConditions(prefix.stripped, ne)»
	«ENDFOR»
	«FOR Definition d : row.writeDefs»
	«IF isArray»
	«CPPDut»setInputArray(«d.getName(row).getDefineNameString», «d.name», index);
	«ELSE»
	«CPPDut»setInput(«d.getName(row).getDefineNameString», «d.name»);
	«ENDIF»
	«ENDFOR»
	#ifndef PSHDL_SIM_NO_BUSCLK_TOGGLE
	if (!«IF cpp»dut->«ENDIF»«DISABLE_EDGES.name») {
		«CPPDut»setInput(busclk_idx, 0);
		«CPPDut»run();
	}
	«CPPDut»setInput(busclk_idx, 1);
	#ifdef PSHDL_SIM_FULL_BUS_WRITE
	int addr=«prefix»getOffset(«prefix»row_«row.name», «IF isArray»index«ELSE»0«ENDIF»);
	uint32_t newVal=«FOR Definition d : row.writeDefs»«d.shifted(row)»«ENDFOR» 0;
	«prefix»writeMemory(base, addr, newVal);
	#else
	«CPPDut»run();
	#endif
	#endif
	return 1;
}

/**
 * Updates the values in memory from the struct. This also advances the simulation by one clock cycle, 
 * unless PSHDL_SIM_NO_BUSCLK_TOGGLE is defined.
 *
 * @param base a (volatile) pointer to the memory offset at which the IP core can be found in memory. For simulation this parameter is ignored.
«IF isArray» * @param index the row that you want to access. Valid values are 0..«rowCount -
		1»«ENDIF»
 * @param newVal the values of this row will be written into the struct
 *
 * @retval 1  Successfully updated the values
 * @retval 0  Something went wrong (invalid index or value exceeds range for example)
 *
 */
int «prefix»set«row.name.toFirstUpper»(uint32_t *base«IF isArray», uint32_t index«ENDIF», «prefix»«row.name»_t *newVal) {
	return «prefix»set«row.name.toFirstUpper»Direct(base«IF isArray», index«ENDIF»«FOR Definition d : row.writeDefs», newVal->«row.getVarNameIndex(d)»«ENDFOR»);
}
'''
	
	def getStripped(String string) {
		if (string.endsWith("_"))
			return string.substring(0, string.length-1)
		return string
	}

	override protected assignNextTime(VariableInformation nextTime, CharSequence currentProcessTime) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

	override protected callMethod(boolean pshdlFunction, CharSequence methodName, CharSequence... args) '''«methodName»(«IF args !== null»«FOR CharSequence arg : args SEPARATOR ','»«arg»«ENDFOR»«ENDIF»)'''

	override protected callRunMethod() {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

	override protected checkTestbenchListener() {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

	override protected runProcessHeader(CommonCodeGenerator.ProcessData pd) {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

	override protected runTestbenchHeader() {
		throw new UnsupportedOperationException("TODO: auto-generated method stub")
	}

	override getHookName() {
		return "C"
	}

	override getUsage() {
		val options = new Options;
		options.addOption("cpp", false, "Generate C++ code")
		return new MultiOption(null, null, options)
	}

	static def List<CompileResult> doCompile(Set<Problem> syntaxProblems, CCodeGeneratorParameter parameter) {
		val comp = new CCodeGenerator(parameter)
		val mainCode=comp.generateMainCode
		val List<AuxiliaryContent> sideFiles = Lists.newLinkedList
		sideFiles.addAll(comp.getAuxiliaryContent(parameter.context))
		return Lists.newArrayList(
			new CompileResult(syntaxProblems, mainCode.toString, parameter.em.moduleName, sideFiles,
				parameter.em.source, comp.hookName, true, if (parameter.cpp) "cpp" else "c"));
	}

	override invoke(CommandLine cli, ExecutableModel em, Set<Problem> syntaxProblems, HDLEvaluationContext context) throws Exception {
		val cgp=new CCodeGeneratorParameter(em, context)
		cgp.cpp = cli.hasOption("cpp")
		doCompile(syntaxProblems, cgp)
	}

	override protected fillArray(VariableInformation vi, CharSequence regFillValue) {
		if (cpp)
			'''std::fill_n(«vi.idName(true, NONE)», «vi.arraySize», «regFillValue»);'''
		else
			'''memset(«vi.idName(true, NONE)», «regFillValue», «vi.arraySize»);'''
	}

	override protected pow(FastInstruction fi, String op, int targetSizeWithType, int pos, int leftOperand,
		int rightOperand, EnumSet<CommonCodeGenerator.Attributes> attributes, boolean doMask) {
		hasPow = true
		return assignTempVar(typeFromTargetSize(targetSizeWithType), targetSizeWithType, pos, NONE,
			'''«CPPMethod»pow(«getTempName(leftOperand, NONE)», «getTempName(rightOperand, NONE)»)''', true)
	}

}
