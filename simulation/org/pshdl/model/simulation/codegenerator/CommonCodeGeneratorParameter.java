package org.pshdl.model.simulation.codegenerator;

import org.pshdl.interpreter.ExecutableModel;
import org.pshdl.model.evaluation.HDLEvaluationContext;

public class CommonCodeGeneratorParameter {
    public final ExecutableModel em;
    public final int bitWidth;

    @Option(description = "A parameter to specify the amount of work per barrier (use Integer.MAX_VALUE to disable)", optionName = "maxCosts", hasArg = true)
    public int maxCosts = Integer.MAX_VALUE;
    @Option(description = "Disable the removal of all variables that are simply renames of other variables.", optionName = "noPurge", hasArg = false)
    public boolean purgeAliases = true;
    public HDLEvaluationContext context;

    public CommonCodeGeneratorParameter(ExecutableModel em, int bitWidth, int maxCosts, boolean purgeAliases, HDLEvaluationContext context) {
        this.em = em;
        this.bitWidth = bitWidth;
        this.maxCosts = maxCosts;
        this.purgeAliases = purgeAliases;
        this.context = context;
    }

    public CommonCodeGeneratorParameter(ExecutableModel em, int bitWidth, HDLEvaluationContext context) {
        this.em = em;
        this.bitWidth = bitWidth;
        this.context = context;
    }
}