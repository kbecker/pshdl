//
//  main.c
//  TestRunner
//
//  Created by Karsten Becker on 28.07.14.
//  Copyright (c) 2014 PSHDL. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <assert.h>
#include "pshdl_generic_sim.hpp"
#include "pshdl_Test_sim.hpp"

Test dut=Test();

void pshdl_assertThat_bool_EAssert_s(bool assumption, uint64_t assertLevel,
		const char* message) {
	if (!assumption) {
		fprintf(stderr, "as %llu '%s'\n", assertLevel, message);
		if (assertLevel < 2) {
			exit(assertLevel+1);
		}
	}
}

int main(int argc, const char *argv[]) {
  uint32_t idx, offset;
  uint64_t value;
  char line[1024];
  printf("#Ready\n");
  fflush(stdout);
  while (fgets(line, 1024, stdin)) {
    switch (line[0]) {
    case '#':
      continue;
    case 'a':
      switch (line[1]) {
      case 'n':
        sscanf(&line[2], "%d %llx", &idx, &value);
        assert(value == dut.getOutput(idx));
        printf(">an\n");
        break;
      case 'a':
        sscanf(&line[2], "%d %d %llx", &idx, &offset, &value);
        assert(value == dut.getOutputArray(idx, offset));
        printf(">aa\n");
        break;
      }
      break;
    case 'd':
      switch (line[1]) {
      case 'j':
        printf(">dj %s\n", dut.getJsonDesc().c_str());
        break;
      case 'c':
        printf(">dc %llu\n", dut.getDeltaCycle());
        break;
      case 'e':
        sscanf(&line[2], "%d", &idx);
        if (idx > 0) {
          dut.setDisableEdges(true);
          printf("#set disabled edges to true\n");
        } else {
          dut.setDisableEdges(false);
          printf("#set disabled edges to false\n");
        }
        printf(">de\n");
        break;
      case 'r':
        sscanf(&line[2], "%d", &idx);
        if (idx > 0) {
          dut.setDisableRegOutputlogic(true);
          printf("#set disabled register output logic to true\n");
        } else {
          dut.setDisableRegOutputlogic(false);
          printf("#set disabled register output logic to false\n");
        }
        printf(">dr\n");
        break;
      }
      break;
    case 'g':
      switch (line[1]) {
      case 'n':
        sscanf(&line[2], "%d", &idx);
        printf(">gn %d %llx\n", idx, dut.getOutput(idx));
        break;
      case 'a':
        sscanf(&line[2], "%d %d", &idx, &offset);
        printf(">ga %d %d %llx\n", idx, offset,
               dut.getOutputArray(idx, offset));
        break;
      }
      break;
    case 'i':
      switch (line[1]) {
      case 'c':
        dut.initConstants();
        printf(">ic\n");
        break;
      }
      break;
    case 'r':
      switch (line[1]) {
      case 'r':
        dut.run();
        printf(">rr\n");
        break;
      }
      break;
    case 's':
      switch (line[1]) {
      case 'n':
        sscanf(&line[2], "%d %llx", &idx, &value);
        printf("#Setting %d to %llx\n", idx, value);
        dut.setInput(idx, value);
        printf(">sn\n");
        break;
      case 'a':
        sscanf(&line[2], "%d %d %llx", &idx, &offset, &value);
        printf("#Setting %d[%d] to %llx\n", idx, offset, value);
        dut.setInputArray(idx, value, offset);
        printf(">sa\n");
        break;
      }
      break;
    case 'x':
      switch (line[1]) {
      case 'n':
        printf(">xn\n");
        fflush(stdout);
        return 0;
      case 'e':
        printf(">xe\n");
        fflush(stdout);
        return 1;
      }
      break;
    }
    fflush(stdout);
  }
  return 2;
}
