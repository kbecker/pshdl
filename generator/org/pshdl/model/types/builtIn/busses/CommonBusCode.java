/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.model.types.builtIn.busses;

import java.util.Map;

import org.pshdl.model.HDLAssignment;
import org.pshdl.model.HDLAssignment.HDLAssignmentType;
import org.pshdl.model.HDLLiteral;
import org.pshdl.model.HDLRange;
import org.pshdl.model.HDLSwitchCaseStatement;
import org.pshdl.model.HDLVariableRef;
import org.pshdl.model.IHDLObject;
import org.pshdl.model.evaluation.HDLEvaluationContext;
import org.pshdl.model.types.builtIn.busses.memorymodel.BlockRam;
import org.pshdl.model.types.builtIn.busses.memorymodel.BlockRam.RWType;
import org.pshdl.model.types.builtIn.busses.memorymodel.Constant;
import org.pshdl.model.types.builtIn.busses.memorymodel.MemoryModel;
import org.pshdl.model.types.builtIn.busses.memorymodel.NamedElement;
import org.pshdl.model.types.builtIn.busses.memorymodel.Row;
import org.pshdl.model.utils.HDLQualifiedName;

public class CommonBusCode {
    public static final String READ = "$read";
    public static final String WRITTEN = "$written";

    public static final String READ_VALUE = "$readValue";
    public static final String ADDRESS = "$address";
    public static final String WRITE_ENABLE = "$writeEnable";
    public static final String WRITE_VALUE = "$writeValue";

    public static HDLSwitchCaseStatement createWriteCase(Row row, Map<String, Integer> intPos, Map<String, Integer> isArray, HDLSwitchCaseStatement label,
            final HDLVariableRef busData) {
        int bitPos = 31;
        for (final NamedElement ne : row.definitions) {
            final BlockRam def = (BlockRam) ne;
            final int size = MemoryModel.getSize(def);
            if (def.writtenFlag) {
                HDLVariableRef target = new HDLVariableRef().setVar(HDLQualifiedName.create(def.getName(row) + WRITTEN));
                target = createRef(intPos, isArray, def, target, row);
                label = label.addDos(new HDLAssignment().setLeft(target).setType(HDLAssignmentType.ASSGN).setRight(HDLLiteral.getTrue()));
            }
            if ((def.rw == RWType.rw) || (def.rw == RWType.w)) {
                HDLVariableRef target = new HDLVariableRef().setVar(HDLQualifiedName.create(def.getName(row)));
                target = createRef(intPos, isArray, def, target, row);
                final HDLRange range = getRange(bitPos, size);
                label = label.addDos(new HDLAssignment().setLeft(target).setType(HDLAssignmentType.ASSGN).setRight(busData.addBits(range)));
            }
            bitPos -= size;
        }
        return label;
    }

    public static HDLSwitchCaseStatement createReadCase(Row row, int reg, Map<String, Integer> intPos, Map<String, Integer> isArray, String dataPort, HDLLiteral labelValue,
            HDLEvaluationContext context, IHDLObject container) {
        HDLSwitchCaseStatement label = new HDLSwitchCaseStatement().setLabel(labelValue);
        final HDLVariableRef target = new HDLVariableRef().setVar(HDLQualifiedName.create(dataPort));
        int bitPos = 31;
        for (final NamedElement ne : row.definitions) {
            final BlockRam def = (BlockRam) ne;
            final int size = MemoryModel.getSize(def);
            if (def.readFlag) {
                HDLVariableRef readTarget = new HDLVariableRef().setVar(HDLQualifiedName.create(def.getName(row) + READ));
                readTarget = createRef(intPos, isArray, def, readTarget, row);
                label = label.addDos(new HDLAssignment().setLeft(readTarget).setType(HDLAssignmentType.ASSGN).setRight(HDLLiteral.getTrue()));
            }
            if ((def.rw == RWType.rw) || (def.rw == RWType.r)) {
                HDLVariableRef source = new HDLVariableRef().setVar(HDLQualifiedName.create(def.getName(row)));
                source = createRef(intPos, isArray, def, source, row);
                final HDLRange range = getRange(bitPos, size);
                label = label.addDos(new HDLAssignment().setLeft(target.addBits(range)).setType(HDLAssignmentType.ASSGN).setRight(source));
            }
            if (def.rw == RWType.constant) {
                final HDLRange range = getRange(bitPos, size);
                final Constant c = (Constant) def;
                label = label.addDos(new HDLAssignment().setLeft(target.addBits(range)).setType(HDLAssignmentType.ASSGN)
                        .setRight(HDLLiteral.get(MemoryModel.dimToInt(context, c.value, container))));
            }
            bitPos -= size;
        }
        return label;
    }

    public static HDLVariableRef createRef(Map<String, Integer> intPos, Map<String, Integer> isArray, BlockRam def, HDLVariableRef source, Row row) {
        final String name = row == null ? def.getName() : def.getName(row);
        final Integer count = isArray.get(name);
        if (count > 1) {
            Integer integer = intPos.get(name);
            if (integer == null) {
                integer = 0;
            }
            source = source.addArray(HDLLiteral.get(integer));
            intPos.put(name, ++integer);
        }
        return source;
    }

    public static HDLRange getRange(int bitPos, int size) {
        HDLRange range = new HDLRange().setTo(HDLLiteral.get(bitPos - (size - 1)));
        if (size != 1) {
            range = range.setFrom(HDLLiteral.get(bitPos));
        }
        return range;
    }

}
