/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.model.types.builtIn.busses;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.RecognitionException;
import org.pshdl.model.HDLAnnotation;
import org.pshdl.model.HDLAssignment;
import org.pshdl.model.HDLAssignment.HDLAssignmentType;
import org.pshdl.model.HDLBitOp;
import org.pshdl.model.HDLBitOp.HDLBitOpType;
import org.pshdl.model.HDLEqualityOp;
import org.pshdl.model.HDLEqualityOp.HDLEqualityOpType;
import org.pshdl.model.HDLExpression;
import org.pshdl.model.HDLForLoop;
import org.pshdl.model.HDLIfStatement;
import org.pshdl.model.HDLInterface;
import org.pshdl.model.HDLLiteral;
import org.pshdl.model.HDLManip;
import org.pshdl.model.HDLManip.HDLManipType;
import org.pshdl.model.HDLObject;
import org.pshdl.model.HDLPackage;
import org.pshdl.model.HDLPrimitive;
import org.pshdl.model.HDLRange;
import org.pshdl.model.HDLRegisterConfig;
import org.pshdl.model.HDLRegisterConfig.HDLRegClockType;
import org.pshdl.model.HDLRegisterConfig.HDLRegResetActiveType;
import org.pshdl.model.HDLStatement;
import org.pshdl.model.HDLSwitchCaseStatement;
import org.pshdl.model.HDLSwitchStatement;
import org.pshdl.model.HDLUnit;
import org.pshdl.model.HDLVariable;
import org.pshdl.model.HDLVariableDeclaration;
import org.pshdl.model.HDLVariableDeclaration.HDLDirection;
import org.pshdl.model.HDLVariableRef;
import org.pshdl.model.IHDLObject;
import org.pshdl.model.evaluation.HDLEvaluationContext;
import org.pshdl.model.types.builtIn.HDLBuiltInAnnotationProvider.HDLBuiltInAnnotations;
import org.pshdl.model.types.builtIn.busses.memorymodel.BlockRam;
import org.pshdl.model.types.builtIn.busses.memorymodel.BlockRam.RWType;
import org.pshdl.model.types.builtIn.busses.memorymodel.MemoryModel;
import org.pshdl.model.types.builtIn.busses.memorymodel.Row;
import org.pshdl.model.types.builtIn.busses.memorymodel.RowOrBlockRam;
import org.pshdl.model.types.builtIn.busses.memorymodel.Unit;
import org.pshdl.model.types.builtIn.busses.memorymodel.v4.MemoryModelAST;
import org.pshdl.model.utils.HDLCore;
import org.pshdl.model.utils.HDLLibrary;
import org.pshdl.model.utils.HDLQualifiedName;
import org.pshdl.model.utils.Insulin;
import org.pshdl.model.validation.Problem;

import com.google.common.base.Charsets;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.io.Files;

public class ABP3BusCodeGen extends CommonBusCode {

	public static final String DIRECT_ADDR = "$directAddr";
	private static final String IS_READY = "$isReady";
	public static final String INT_PORT = "$int_port";
	public static final String APB_PORT = "$apb_port";
	private static final String PCLK = "PCLK";
	private static final String PRESETN = "PRESETn";
	private static final String PREADY = "PREADY";
	private static final String PSLVERR = "PSLVERR";
	private static final String PENABLE = "PENABLE";
	private static final String PWRITE = "PWRITE";
	private static final String PSEL = "PSEL";
	private static final String PWDATA = "PWDATA";
	private static final String PWDATA_TMP = "PWDATA_TMP";
	private static final String PADDR = "PADDR";
	private static final String PRDATA = "PRDATA";
	private static final String PRDATA_TMP = "PRDATA_TMP";

	public static HDLUnit get(String name, Unit unit, List<RowOrBlockRam> rows, boolean defaultClock, boolean defaultReset, boolean readDelay, final HDLEvaluationContext context,
			IHDLObject container) {
		final HDLInterface hdi = MemoryModel.buildHDLInterface(unit, rows, context).setContainer(container);
		final Map<String, Integer> isArray = MemoryModel.getDefCount(rows);
		final int addrWidth = bitCount(unit.lastBase * 4);
		final HDLVariable pclkVar = new HDLVariable().setName(PCLK);
		HDLVariableDeclaration pclk = new HDLVariableDeclaration().setDirection(HDLDirection.IN).setType(HDLPrimitive.getBit()).addVariables(pclkVar);
		if (defaultClock) {
			pclk = pclk.addAnnotations(HDLBuiltInAnnotations.clock.create(null));
		}
		final HDLVariable presetVar = new HDLVariable().setName(PRESETN);
		final HDLVariableDeclaration preset = new HDLVariableDeclaration().setDirection(HDLDirection.INTERNAL).setType(HDLPrimitive.getBit())
				.addAnnotations(HDLBuiltInAnnotations.reset.create(null))
				.addVariables(new HDLVariable().setName("rst").setDefaultValue(new HDLManip().setType(HDLManipType.BIT_NEG).setTarget(presetVar.asHDLRef())));
		final RegisterParameter resetInfo = new RegisterParameter(defaultClock, pclkVar, defaultReset, presetVar, null);
		HDLUnit res = new HDLUnit().setName(name).addStatements(pclk)
				.addStatements(new HDLVariableDeclaration().setDirection(HDLDirection.IN).setType(HDLPrimitive.getBit()).addVariables(presetVar))
				.addStatements(new HDLVariableDeclaration().setDirection(HDLDirection.IN).setType(HDLPrimitive.getBit()).addVariables(new HDLVariable().setName(PENABLE)))
				.addStatements(new HDLVariableDeclaration().setDirection(HDLDirection.IN).setType(HDLPrimitive.getBit()).addVariables(new HDLVariable().setName(PSEL)))
				.addStatements(
						new HDLVariableDeclaration().setDirection(HDLDirection.IN).setType(HDLPrimitive.getBitvector(addrWidth)).addVariables(new HDLVariable().setName(PADDR)))
				.addStatements(new HDLVariableDeclaration().setDirection(HDLDirection.IN).setType(HDLPrimitive.getBit()).addVariables(new HDLVariable().setName(PWRITE)))
				.addStatements(new HDLVariableDeclaration().setDirection(HDLDirection.IN).setType(HDLPrimitive.getBitvector(32)).addVariables(new HDLVariable().setName(PWDATA)))
				.addStatements(new HDLVariableDeclaration().setRegister(createRegister(resetInfo)).setType(HDLPrimitive.getBitvector(32))
						.addVariables(new HDLVariable().setName(PWDATA_TMP).setDefaultValue(createRef(PWDATA))))
				.addStatements(new HDLVariableDeclaration().setRegister(createRegister(resetInfo)).setDirection(HDLDirection.OUT).setType(HDLPrimitive.getBitvector(32))
						.addVariables(new HDLVariable().setName(PRDATA).setDefaultValue(readDelay ? createRef(PRDATA_TMP) : null)))
				.addStatements(
						new HDLVariableDeclaration().setDirection(HDLDirection.OUT).setType(HDLPrimitive.getBit())
								.addVariables(new HDLVariable().setName(PREADY).setDefaultValue(new HDLLiteral().setVal("1"))))
				.addStatements(
						new HDLVariableDeclaration()
								.setDirection(HDLDirection.OUT).setType(
										HDLPrimitive.getBit())
								.addVariables(new HDLVariable().setName(PSLVERR).setDefaultValue(new HDLLiteral().setVal("0"))))
				.addStatements(
						new HDLIfStatement()
								.setIfExp(
										new HDLBitOp().setLeft(createRef(PWRITE)).setRight(new HDLVariableRef().setVar(HDLQualifiedName.create(PSEL)))
												.setType(HDLBitOpType.LOGI_AND))
								.addThenDo(createWriteSwitch(unit, rows, isArray)))
				.addStatements(new HDLIfStatement()
						.setIfExp(new HDLBitOp().setLeft(new HDLEqualityOp().setLeft(createRef(PWRITE)).setRight(new HDLLiteral().setVal("0")).setType(HDLEqualityOpType.EQ))
								.setRight(createRef(PSEL)).setType(HDLBitOpType.LOGI_AND))
						.setThenDo(createReadSwitch(unit, rows, isArray, resetInfo, readDelay, context, container))
//                        .addElseDo(new HDLAssignment().setLeft(createRef(PRDATA)).setType(HDLAssignmentType.ASSGN).setRight(new HDLLiteral().setVal("0")))
				).setSimulation(false);
		if (defaultReset) {
			res = res.addStatements(preset);
		}
		if (readDelay) {
			res = res.addInits(
					new HDLVariableDeclaration().setRegister(createRegister(resetInfo)).setType(HDLPrimitive.getBitvector(32)).addVariables(new HDLVariable().setName(PRDATA_TMP)));
		}
		final Set<String> declaredRAMs = Sets.newHashSet();
		for (final RowOrBlockRam rowOrBlockRam : rows) {
			if (rowOrBlockRam instanceof BlockRam) {
				final BlockRam ram = (BlockRam) rowOrBlockRam;
				if (!declaredRAMs.contains(ram.name)) {
					declaredRAMs.add(ram.name);
					final int dim = isArray.get(ram.name);
					final int ramSize = MemoryModel.dimToInt(context, ram.dimension, container);
					final HDLAnnotation memAnno = HDLBuiltInAnnotations.memory.create(ram.name);
					HDLVariable internalPort = new HDLVariable().setName(ram.name + INT_PORT).addDimensions(HDLLiteral.get(ramSize)).addAnnotations(memAnno);
					HDLVariable apbPort = new HDLVariable().setName(ram.name + APB_PORT).addDimensions(HDLLiteral.get(ramSize));
					if (readDelay) {
						apbPort = apbPort.addAnnotations(HDLBuiltInAnnotations.memory.create(ram.name + ";noReadRegister"));
					} else {
						apbPort = apbPort.addAnnotations(memAnno);
					}
					if (dim != 1) {
						apbPort = apbPort.addDimensions(HDLLiteral.get(dim));
						internalPort = internalPort.addDimensions(HDLLiteral.get(dim));
					}
					final HDLRegisterConfig register = createRegister(resetInfo);
					final HDLPrimitive ramType = MemoryModel.getType(ram);
					final HDLVariableDeclaration portDecl = new HDLVariableDeclaration().setRegister(register).setType(ramType).addVariables(apbPort).addVariables(internalPort);

					final HDLVariableRef address = createRef(ram.name + CommonBusCode.ADDRESS);
					final HDLVariableRef writeValue = createRef(ram.name + CommonBusCode.WRITE_VALUE);
					final HDLVariableRef intPortAddrRef = internalPort.asHDLRef().addArray(address);
					final HDLAssignment write = new HDLAssignment().setLeft(intPortAddrRef).setRight(writeValue);
					final HDLVariableRef writeEnable = createRef(ram.name + CommonBusCode.WRITE_ENABLE);
					HDLStatement ifWrite = null;
					if (dim != 1) {
						final HDLVariable I = new HDLVariable().setName(ram.name + "_$I");
						final HDLIfStatement ifCase = new HDLIfStatement().setIfExp(writeEnable.addArray(I.asHDLRef()))
								.addThenDo(write.setLeft(intPortAddrRef.addArray(I.asHDLRef())).setRight(writeValue.addArray(I.asHDLRef())));
						ifWrite = new HDLForLoop().setParam(I).setRange(HDLObject.asList(new HDLRange().setFrom(HDLLiteral.get(0)).setTo(HDLLiteral.get(dim)))).addDos(ifCase);
					} else {
						ifWrite = new HDLIfStatement().setIfExp(writeEnable).addThenDo(write);
					}
					final HDLAssignment read = new HDLAssignment().setLeft(createRef(ram.name + CommonBusCode.READ_VALUE)).setRight(HDLManip.getCast(ramType, intPortAddrRef));
					res = res.addStatements(portDecl).addStatements(ifWrite).addStatements(read);
				}
			}
		}
		for (final HDLVariableDeclaration port : hdi.getPorts()) {
			HDLRegisterConfig register = null;
			if (port.getRegister() != null) {
				register = createRegister(new RegisterParameter(defaultClock, pclkVar, defaultReset, presetVar, port.getRegister().getResetValue()));
			}
			res = res.addStatements(port.setDirection(HDLDirection.INTERNAL).setRegister(register));
		}
		return res;
	}

	public static class RegisterParameter {
		public boolean defaultClock;
		public HDLVariable clockVar;
		public boolean defaultReset;
		public HDLVariable resetVar;
		public HDLExpression resetValue;

		public RegisterParameter(boolean defaultClock, HDLVariable clockVar, boolean defaultReset, HDLVariable resetVar, HDLExpression resetValue) {
			this.defaultClock = defaultClock;
			this.clockVar = clockVar;
			this.defaultReset = defaultReset;
			this.resetVar = resetVar;
			this.resetValue = resetValue;
		}
	}

	private static HDLRegisterConfig createRegister(RegisterParameter resetInfo) {
		HDLRegisterConfig config = HDLRegisterConfig.defaultConfig().setClockType(HDLRegClockType.RISING)
				.setResetValue(resetInfo.resetValue == null ? HDLLiteral.get(0) : resetInfo.resetValue);
		if (!resetInfo.defaultClock) {
			config = config.setClk(resetInfo.clockVar.asHDLRef());
		}
		if (!resetInfo.defaultReset) {
			config = config.setRst(resetInfo.resetVar.asHDLRef()).setResetType(HDLRegResetActiveType.LOW);
		}
		return config;
	}

	private static HDLStatement createWriteSwitch(Unit unit, List<RowOrBlockRam> rows, Map<String, Integer> isArray) {
		final LinkedHashMap<String, Integer> intPos = Maps.newLinkedHashMap();
		if (unit.hasBlockRam) {
			final int blocks = unit.lastBase / unit.biggestBlockRam;
			final int blockBits = bitCount(blocks);
			final int subBits = bitCount(unit.biggestBlockRam - 1) + 2;
			final HDLRange upperRange = new HDLRange().setFrom(HDLLiteral.get((blockBits + subBits) - 1)).setTo(HDLLiteral.get(subBits));
			final HDLRange subRange = new HDLRange().setFrom(HDLLiteral.get(subBits - 1)).setTo(HDLLiteral.get(0));
			final HDLVariableRef upperBits = createRef(PADDR).addBits(upperRange);
			HDLSwitchStatement hsl = new HDLSwitchStatement().setCaseExp(upperBits);
			final List<RowOrBlockRam> subList = Lists.newArrayList();
			int block = 0;
			for (final RowOrBlockRam rowOrBlockRam : rows) {
				if (rowOrBlockRam instanceof Row) {
					final Row row = (Row) rowOrBlockRam;
					subList.add(row);
					if (subList.size() >= (unit.biggestBlockRam)) {
						final int block1 = block;
						final HDLSwitchStatement regularWrite = generateRegularWrite(subList, isArray, subRange, intPos);
						if (regularWrite.getCases().size() != 1) {
							final HDLIfStatement ifEnabled = new HDLIfStatement().setIfExp(createRef(PENABLE)).setThenDo(HDLObject.asList(regularWrite));
							hsl = hsl.addCases(new HDLSwitchCaseStatement().setLabel(HDLLiteral.get(block1)).addDos(ifEnabled));
						}
						subList.clear();
						block++;
					}
				} else {
					final BlockRam ram = (BlockRam) rowOrBlockRam;
					if (ram.rw == RWType.r) {
						block++;
						continue;
					}
					String dim = ram.dimension;
					if (dim.startsWith("$")) {
						dim = dim.substring(1);
					}
					final HDLVariable directAddr = new HDLVariable().setName(ram.name + DIRECT_ADDR).setDefaultValue(createRef(PADDR).addBits(subRange.setTo(HDLLiteral.get(2))));
					final HDLVariableDeclaration directVar = new HDLVariableDeclaration().setType(HDLPrimitive.getNatural()).addVariables(directAddr)
							.addAnnotations(HDLBuiltInAnnotations.range.create("0;" + dim + "-1"));
					HDLVariableRef arrayWrite = createRef(ram.name + APB_PORT).addArray(directAddr.asHDLRef());
					if (isArray.get(ram.name) > 1) {
						Integer arr = intPos.get(ram.name);
						if (arr == null) {
							arr = 0;
						}
						arrayWrite = arrayWrite.addArray(HDLLiteral.get(arr));
						intPos.put(ram.name, ++arr);
					}

					final HDLAssignment write = new HDLAssignment().setLeft(arrayWrite).setRight(createRef(PWDATA));
					final HDLIfStatement ifEnabled = new HDLIfStatement().setIfExp(createRef(PENABLE)).addThenDo(write);
					HDLSwitchCaseStatement caseStmnt = new HDLSwitchCaseStatement().setLabel(HDLLiteral.get(block)).addDos(directVar).addDos(ifEnabled);
					if (ram.writtenFlag) {
						HDLVariableRef target = createRef(ram.getName() + CommonBusCode.WRITTEN);
						target = createRef(intPos, isArray, ram, target, null);
						caseStmnt = caseStmnt.addDos(new HDLAssignment().setLeft(target).setType(HDLAssignmentType.ASSGN).setRight(HDLLiteral.getTrue()));
					}
					hsl = hsl.addCases(caseStmnt);
					block++;
				}
			}
			if (subList.size() != 0) {
				final HDLSwitchStatement regularWrite = generateRegularWrite(subList, isArray, subRange, intPos);
				if (regularWrite.getCases().size() != 1) {
					hsl = hsl.addCases(
							new HDLSwitchCaseStatement().setLabel(HDLLiteral.get(block)).addDos(new HDLIfStatement().setIfExp(createRef(PENABLE)).addThenDo(regularWrite)));
				}
				subList.clear();
			}
			return hsl.addCases(new HDLSwitchCaseStatement().setLabel(null));
		}
		return new HDLIfStatement().setIfExp(createRef(PENABLE)).setThenDo(HDLObject.asList(generateRegularWrite(rows, isArray, null, intPos)));
	}

	public static HDLSwitchStatement generateRegularWrite(List<RowOrBlockRam> rows, Map<String, Integer> isArray, HDLRange bits, final LinkedHashMap<String, Integer> intPos) {
		HDLVariableRef labelVar = createRef(PADDR);
		if (bits != null) {
			labelVar = labelVar.addBits(bits);
		}
		HDLSwitchStatement hsl = new HDLSwitchStatement().setCaseExp(labelVar);
		int pos = 0;
		for (final RowOrBlockRam row : rows) {
			final HDLSwitchCaseStatement writeCase = createWriteCase((Row) row, pos++, intPos, isArray);
			if (!writeCase.getDos().isEmpty()) {
				hsl = hsl.addCases(writeCase);
			}
		}
		return hsl.addCases(new HDLSwitchCaseStatement().setLabel(null));
	}

	public static int bitCount(int blocks) {
		int blockBits = 0;
		while (blocks != 0) {
			blockBits++;
			blocks >>>= 1;
		}
		return blockBits;
	}

	private static HDLSwitchCaseStatement createWriteCase(Row row, int pos, Map<String, Integer> intPos, Map<String, Integer> isArray) {
		HDLSwitchCaseStatement label = new HDLSwitchCaseStatement().setLabel(HDLLiteral.get(pos * 4l));
		final HDLVariableRef busData = createRef(PWDATA_TMP);
		label = createWriteCase(row, intPos, isArray, label, busData);
		return label;
	}

	private static Iterable<HDLStatement> createReadSwitch(Unit unit, List<RowOrBlockRam> rows, Map<String, Integer> isArray, RegisterParameter resetInfo, final boolean delayRead,
			HDLEvaluationContext context, IHDLObject container) {
		final LinkedHashMap<String, Integer> intPos = Maps.newLinkedHashMap();
		boolean needsReadyVar = delayRead;
		HDLSwitchStatement finalSwitchCase;
		String busDataTarget = PRDATA;
		if (delayRead) {
			busDataTarget = PRDATA_TMP;
		}
		if (unit.hasBlockRam) {
			final int blocks = unit.lastBase / unit.biggestBlockRam;
			final int blockBits = bitCount(blocks);
			final int subBits = bitCount(unit.biggestBlockRam - 1) + 2;
			final HDLRange upperRange = new HDLRange().setFrom(HDLLiteral.get((blockBits + subBits) - 1)).setTo(HDLLiteral.get(subBits));
			final HDLRange subRange = new HDLRange().setFrom(HDLLiteral.get(subBits - 1)).setTo(HDLLiteral.get(0));
			final HDLVariableRef upperBits = createRef(PADDR).addBits(upperRange);
			HDLSwitchStatement hsl = new HDLSwitchStatement().setCaseExp(upperBits);
			final List<RowOrBlockRam> subList = Lists.newArrayList();
			int block = 0;
			for (final RowOrBlockRam rowOrBlockRam : rows) {
				if (rowOrBlockRam instanceof Row) {
					final Row row = (Row) rowOrBlockRam;
					subList.add(row);
					if (subList.size() >= (unit.biggestBlockRam)) {
						hsl = encapsulateReadSwitch(hsl, block, generateRegularRead(subList, isArray, subRange, intPos, context, container, busDataTarget));
						subList.clear();
						block++;
					}
				} else {
					final BlockRam ram = (BlockRam) rowOrBlockRam;
					if (ram.rw == RWType.w) {
						block++;
						continue;
					}
					String dim = ram.dimension;
					if (dim.startsWith("$")) {
						dim = dim.substring(1);
					}
					final HDLVariable directAddr = new HDLVariable().setName(ram.name + DIRECT_ADDR).setDefaultValue(createRef(PADDR).addBits(subRange.setTo(HDLLiteral.get(2))));
					final HDLVariableDeclaration directVar = new HDLVariableDeclaration().setType(HDLPrimitive.getNatural()).addVariables(directAddr)
							.addAnnotations(HDLBuiltInAnnotations.range.create("0;" + dim + "-1"));

					HDLVariableRef arrayRead = createRef(ram.name + APB_PORT).addArray(directAddr.asHDLRef());
					if (isArray.get(ram.name) > 1) {
						Integer arr = intPos.get(ram.name);
						if (arr == null) {
							arr = 0;
						}
						arrayRead = arrayRead.addArray(HDLLiteral.get(arr));
						intPos.put(ram.name, ++arr);
					}

					final HDLAssignment read = new HDLAssignment().setLeft(createRef(busDataTarget)).setRight(arrayRead);
					needsReadyVar = true;
					HDLSwitchCaseStatement caseStmnt = new HDLSwitchCaseStatement().setLabel(HDLLiteral.get(block)).addDos(directVar).addDos(read);
					if (ram.readFlag) {
						HDLVariableRef target = createRef(ram.getName() + CommonBusCode.READ);
						target = createRef(intPos, isArray, ram, target, null);
						caseStmnt = caseStmnt.addDos(new HDLAssignment().setLeft(target).setType(HDLAssignmentType.ASSGN).setRight(HDLLiteral.getTrue()));
					}
					hsl = hsl.addCases(caseStmnt);
					block++;
				}
			}
			if (!subList.isEmpty()) {
				hsl = encapsulateReadSwitch(hsl, block, generateRegularRead(subList, isArray, subRange, intPos, context, container, busDataTarget));
			}
			finalSwitchCase = hsl;
		} else {
			finalSwitchCase = generateRegularRead(rows, isArray, null, intPos, context, container, busDataTarget);
		}
		final List<HDLStatement> res = new ArrayList<>();
		if (needsReadyVar) {
			final HDLVariable isReadyVar = new HDLVariable().setName(IS_READY).setDefaultValue(HDLLiteral.get(0));
			final HDLAssignment setReady = new HDLAssignment().setLeft(isReadyVar.asHDLRef()).setRight(HDLLiteral.get(1));
			res.add(new HDLVariableDeclaration().addVariables(isReadyVar).setType(HDLPrimitive.getBit()).setRegister(createRegister(resetInfo)));
			res.add(new HDLAssignment().setLeft(createRef(PREADY)).setRight(isReadyVar.asHDLRef()));
			res.add(new HDLIfStatement().setIfExp(createRef(PENABLE)).addThenDo(setReady));
		}
		res.add(finalSwitchCase.addCases(new HDLSwitchCaseStatement().setLabel(null)));
		return res;
	}

	private static HDLSwitchStatement encapsulateReadSwitch(HDLSwitchStatement hsl, int block, HDLSwitchStatement regularRead) {
		if (regularRead.getCases().size() != 1) {
			hsl = hsl.addCases(new HDLSwitchCaseStatement().setLabel(HDLLiteral.get(block)).addDos(regularRead.addCases(new HDLSwitchCaseStatement())));
		}
		return hsl;
	}

	public static HDLVariableRef createRef(String varName) {
		return new HDLVariableRef().setVar(HDLQualifiedName.create(varName));
	}

	public static HDLSwitchStatement generateRegularRead(List<RowOrBlockRam> rows, Map<String, Integer> isArray, HDLRange bits, final LinkedHashMap<String, Integer> intPos,
			HDLEvaluationContext context, IHDLObject container, String busDataTarget) {
		HDLVariableRef labelVar = createRef(PADDR);
		if (bits != null) {
			labelVar = labelVar.addBits(bits);
		}
		HDLSwitchStatement res = new HDLSwitchStatement().setCaseExp(labelVar);
		int pos = 0;
		for (final RowOrBlockRam row : rows) {
			final int reg = pos++;
			final HDLSwitchCaseStatement readCase = createReadCase((Row) row, reg, intPos, isArray, busDataTarget, HDLLiteral.get(reg * 4), context, container);
			if (!readCase.getDos().isEmpty()) {
				res = res.addCases(readCase);
			}
		}
		return res;
	}

	public static void main(String[] args) throws FileNotFoundException, IOException, RecognitionException {
		final Unit unit = MemoryModelAST.parseUnit(Files.toString(new File(args[0]), Charsets.UTF_8), new LinkedHashSet<Problem>(), 0);
		System.out.println(unit);
		HDLEvaluationContext context = new HDLEvaluationContext();
		context = context.set("BRAM_SIZE", HDLLiteral.get(640));
		context = context.set("MOTORS", HDLLiteral.get(10));
		context = context.set("NUM_REGS", HDLLiteral.get(64));
		final HDLUnit hdlUnit = get("Bla", unit, MemoryModel.buildRows(unit, context, null), true, false, true, context, null);
		System.out.println(hdlUnit);
		HDLCore.defaultInit();
		final HDLPackage copyDeepFrozen = new HDLPackage().setLibURI("gen.pshdl").addUnits(hdlUnit.setLibURI("gen.pshdl")).copyDeepFrozen(null);
		final HDLLibrary library = new HDLLibrary();
		HDLLibrary.registerLibrary("gen.pshdl", library);
		final HDLPackage transform = Insulin.transform(copyDeepFrozen, "gen.pshdl", context);
		System.out.println(transform);
	}
}
