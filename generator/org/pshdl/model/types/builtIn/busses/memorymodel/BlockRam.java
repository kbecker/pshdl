package org.pshdl.model.types.builtIn.busses.memorymodel;

import org.antlr.v4.runtime.Token;

public class BlockRam implements NamedElement, RowOrBlockRam {

    public enum Type {
        BIT, INT, UINT, UNUSED
    }

    public enum RWType {
        r, rw, w, constant;
    }

    public String dimension;
    public String name = "fill";
    public boolean writtenFlag;
    public boolean readFlag;
    public RWType rw = null;
    public Type type = Type.UNUSED;
    public int width = -1;
    public int address;
    public Token token;

    @Override
    public String getName() {
        return name;
    }

    public String getName(Row row) {
        if (row.isHidden()) {
            return name;
        }
        return row.getSimpleName() + '_' + name;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append('[').append(dimension).append(']');
        final String w = width != -1 ? "<" + width + ">" : "";
        final String lowerCase = type.name().toLowerCase();
        final String rwString = rw != null ? rw + " " : "";
        if (type == Type.UNUSED) {
            return name + w + ";";
        }
        String mod = "";
        if (writtenFlag) {
            mod = " writtenFlag";
        }
        if (readFlag) {
            mod = " readFlag";
        }
        return rwString + "blockRam " + lowerCase + w + " " + name + sb + " " + mod + ";";
    }

    public BlockRam withoutDim() {
        final BlockRam res = new BlockRam();
        res.type = type;
        res.width = width;
        res.name = name;
        res.rw = rw;
        res.writtenFlag = writtenFlag;
        res.readFlag = readFlag;
        return res;
    }

    public BlockRam() {
        super();
    }

    @Override
    public void setLocation(Token start) {
        this.token = start;
    }

    @Override
    public String getSimpleName() {
        return getName();
    }

    @Override
    public int getAddress() {
        return address;
    }

    @Override
    public void setAddress(int baseAddr) {
        this.address = baseAddr;
    }

}