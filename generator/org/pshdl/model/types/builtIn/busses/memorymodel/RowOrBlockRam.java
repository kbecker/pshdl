package org.pshdl.model.types.builtIn.busses.memorymodel;

public interface RowOrBlockRam extends NamedElement {
    int getAddress();

    void setAddress(int baseAddr);
}
