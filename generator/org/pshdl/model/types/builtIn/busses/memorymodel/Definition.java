/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.model.types.builtIn.busses.memorymodel;

public class Definition extends BlockRam implements NamedElement {
    public enum WarnType {
        mask, silentMask, limit, silentLimit, error, silentError;

        @Override
        public String toString() {
            switch (this) {
            case limit:
                return "limit";
            case silentLimit:
                return "silent limit";
            case mask:
                return "mask";
            case silentMask:
                return "silent mask";
            case error:
                return "error";
            case silentError:
                return "silent error";
            default:
                throw new IllegalArgumentException("Did not correctly implement this:" + this);
            }
        }
    }

    public boolean register;

    public WarnType warn = WarnType.limit;

    public int bitPos;

    public String resetValue;

    public Integer arrayIndex;

    public Definition() {
    }

    public Definition(String name, String resetValue, boolean register, boolean writtenFlag, boolean readFlag, RWType rw, Type type, int width, String dimension) {
        super();
        this.dimension = dimension;
        this.name = name;
        this.resetValue = resetValue;
        this.register = register;
        this.writtenFlag = writtenFlag;
        this.readFlag = readFlag;
        this.rw = rw;
        this.type = type;
        this.width = width;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Definition other = (Definition) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        if (readFlag != other.readFlag) {
            return false;
        }
        if (register != other.register) {
            return false;
        }
        if (resetValue == null) {
            if (other.resetValue != null) {
                return false;
            }
        } else if (!resetValue.equals(other.resetValue)) {
            return false;
        }
        if (rw != other.rw) {
            return false;
        }
        if (type != other.type) {
            return false;
        }
        if (warn != other.warn) {
            return false;
        }
        if (width != other.width) {
            return false;
        }
        if (writtenFlag != other.writtenFlag) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((name == null) ? 0 : name.hashCode());
        result = (prime * result) + (readFlag ? 1231 : 1237);
        result = (prime * result) + (register ? 1231 : 1237);
        result = (prime * result) + ((resetValue == null) ? 0 : resetValue.hashCode());
        result = (prime * result) + ((rw == null) ? 0 : rw.hashCode());
        result = (prime * result) + ((type == null) ? 0 : type.hashCode());
        result = (prime * result) + ((warn == null) ? 0 : warn.hashCode());
        result = (prime * result) + width;
        result = (prime * result) + (writtenFlag ? 1231 : 1237);
        return result;
    }

    @Override
    public String toString() {
        String array = "";
        if (dimension != null) {
            array = '[' + dimension + ']';
        }
        final String reg = register ? "register " : "";
        final String w = width != -1 ? "<" + width + ">" : "";
        final String lowerCase = type.name().toLowerCase();
        final String rwString = rw != null ? rw + " " : "";
        if (type == Type.UNUSED) {
            return name + w + ";";
        }
        String mod = "";
        if (writtenFlag) {
            mod = " writtenFlag";
        }
        if (readFlag) {
            mod = " readFlag";
        }
        String reset = "";
        if (resetValue != null) {
            reset = " = " + resetValue;
        }
        return rwString + reg + lowerCase + w + " " + name + array + reset + " " + warn + mod + ";";
    }

    @Override
    public Definition withoutDim() {
        final Definition res = new Definition();
        res.type = type;
        res.width = width;
        res.name = name;
        res.resetValue = resetValue;
        res.rw = rw;
        res.register = register;
        res.warn = warn;
        res.writtenFlag = writtenFlag;
        res.readFlag = readFlag;
        return res;
    }
}
