/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
package org.pshdl.model.types.builtIn.busses.memorymodel;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.pshdl.model.HDLExpression;
import org.pshdl.model.HDLInterface;
import org.pshdl.model.HDLLiteral;
import org.pshdl.model.HDLPrimitive;
import org.pshdl.model.HDLRegisterConfig;
import org.pshdl.model.HDLVariable;
import org.pshdl.model.HDLVariableDeclaration;
import org.pshdl.model.HDLVariableDeclaration.HDLDirection;
import org.pshdl.model.IHDLObject;
import org.pshdl.model.evaluation.ConstantEvaluate;
import org.pshdl.model.evaluation.HDLEvaluationContext;
import org.pshdl.model.parser.PSHDLParser;
import org.pshdl.model.types.builtIn.busses.CommonBusCode;
import org.pshdl.model.types.builtIn.busses.memorymodel.BlockRam.Type;
import org.pshdl.model.types.builtIn.busses.memorymodel.v4.MemoryModelAST;
import org.pshdl.model.utils.LazyHDLInterface;
import org.pshdl.model.validation.Problem;

import com.google.common.base.Charsets;
import com.google.common.base.Optional;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.io.Files;

public class MemoryModel {

    private static final class LazyHDLGenerationInterface extends LazyHDLInterface {

        private final Unit unit;
        private final List<RowOrBlockRam> rows;
        private final HDLEvaluationContext context;

        protected LazyHDLGenerationInterface(int id, IHDLObject container, String name, Iterable<HDLExpression> dim, boolean validate, Unit unit, List<RowOrBlockRam> rows,
                HDLEvaluationContext context) {
            super(id, container, name, dim, validate);
            this.unit = unit;
            this.rows = rows;
            this.context = context;
        }

        public LazyHDLGenerationInterface(Unit unit, List<RowOrBlockRam> rows, HDLEvaluationContext context) {
            this.unit = unit;
            this.rows = rows;
            this.context = context;
        }

        @Override
        protected ArrayList<HDLVariableDeclaration> getLazyPorts() {
            final ArrayList<HDLVariableDeclaration> cachedPorts = new ArrayList<>();
            final Map<String, RowOrBlockRam> definitions = Maps.newLinkedHashMap();
            final Map<String, Integer> defDimension = Maps.newLinkedHashMap();
            for (final RowOrBlockRam block_row : rows) {
                if (block_row instanceof Row) {
                    final Row row = (Row) block_row;
                    for (final NamedElement ne : row.definitions) {
                        if (ne instanceof Constant) {
                            continue;
                        }
                        final Definition def = (Definition) ne;
                        final String name = def.getName(row);
                        final RowOrBlockRam stockDef = definitions.get(name);
                        if ((stockDef != null) && !def.equals(stockDef) && (def.type != Type.UNUSED)) {
                            throw new IllegalArgumentException("Two definitions with same name exist, but their type differs:" + def + " vs " + stockDef);
                        }
                        addDefintion(definitions, defDimension, def, row);
                    }
                } else {
                    final BlockRam ram = (BlockRam) block_row;
                    addDefintion(definitions, defDimension, ram, null);
                }
            }
            for (final Entry<String, RowOrBlockRam> entry : definitions.entrySet()) {
                final RowOrBlockRam row = entry.getValue();
                if (row instanceof Definition) {
                    final Definition def = (Definition) row;
                    if (def.type == Type.UNUSED) {
                        continue;
                    }
                    final String name = entry.getKey();
                    final HDLPrimitive type = MemoryModel.getType(def);
                    HDLVariableDeclaration hdv = new HDLVariableDeclaration().setType(type);
                    HDLRegisterConfig register = def.register ? HDLRegisterConfig.defaultConfig() : null;
                    HDLVariable var = new HDLVariable().setName(name);
                    if (def.resetValue != null) {
                        final HDLLiteral resetVal = HDLLiteral.get(dimToInt(context, def.resetValue, register));
                        if (register != null) {
                            register = register.setResetValue(resetVal);
                        } else {
                            var = var.setDefaultValue(resetVal);
                        }
                    }
                    hdv = hdv.setRegister(register);
                    switch (def.rw) {
                    case r:
                        hdv = hdv.setDirection(HDLDirection.IN);
                        break;
                    case rw:
                        hdv = hdv.setDirection(HDLDirection.INOUT);
                        break;
                    case w:
                        hdv = hdv.setDirection(HDLDirection.OUT);
                        break;
                    case constant:
                        throw new IllegalArgumentException("Constants don't have a variable");
                    }
                    final Integer dim = defDimension.get(name);
                    if (dim != 1) {
                        var = var.addDimensions(HDLLiteral.get(dim));
                    }
                    hdv = hdv.addVariables(var);
                    cachedPorts.add(hdv);
                    if (def.writtenFlag) {
                        HDLVariableDeclaration hvd = new HDLVariableDeclaration().setType(HDLPrimitive.getBool()).setRegister(HDLRegisterConfig.defaultConfig())
                                .setDirection(HDLDirection.OUT);
                        hvd = hvd.addVariables(new HDLVariable().setName(name + CommonBusCode.WRITTEN).setDefaultValue(HDLLiteral.getFalse()));
                        cachedPorts.add(hvd);
                    }
                    if (def.readFlag) {
                        HDLVariableDeclaration hvd = new HDLVariableDeclaration().setType(HDLPrimitive.getBool()).setRegister(HDLRegisterConfig.defaultConfig())
                                .setDirection(HDLDirection.OUT);
                        hvd = hvd.addVariables(new HDLVariable().setName(name + CommonBusCode.READ).setDefaultValue(HDLLiteral.getFalse()));
                        cachedPorts.add(hvd);
                    }
                } else {
                    final BlockRam ram = (BlockRam) row;
                    HDLLiteral dim = null;
                    final Integer dimInt = defDimension.get(ram.getName());
                    if (dimInt != 1) {
                        dim = HDLLiteral.get(dimInt);
                    }
                    addBlockRamPorts(cachedPorts, ram, dim);
                    if (ram.writtenFlag) {
                        HDLVariableDeclaration hvd = new HDLVariableDeclaration().setType(HDLPrimitive.getBool()).setRegister(HDLRegisterConfig.defaultConfig())
                                .setDirection(HDLDirection.OUT);
                        hvd = hvd.addVariables(new HDLVariable().setName(ram.name + CommonBusCode.WRITTEN).setDefaultValue(HDLLiteral.getFalse()));
                        cachedPorts.add(hvd);
                    }
                    if (ram.readFlag) {
                        HDLVariableDeclaration hvd = new HDLVariableDeclaration().setType(HDLPrimitive.getBool()).setRegister(HDLRegisterConfig.defaultConfig())
                                .setDirection(HDLDirection.OUT);
                        hvd = hvd.addVariables(new HDLVariable().setName(ram.name + CommonBusCode.READ).setDefaultValue(HDLLiteral.getFalse()));
                        cachedPorts.add(hvd);
                    }
                }
            }
            return cachedPorts;
        }

        void addDefintion(Map<String, RowOrBlockRam> definitions, Map<String, Integer> defDimension, BlockRam def, Row row) {
            final String name = row != null ? def.getName(row) : def.getName();
            definitions.put(name, def);
            final Integer val = defDimension.get(name);
            if (val == null) {
                defDimension.put(name, 1);
            } else {
                defDimension.put(name, val + 1);
            }
        }

        public void addBlockRamPorts(final List<HDLVariableDeclaration> cachedPorts, final BlockRam ram, HDLLiteral dim) {
            final String name = ram.name;
            final HDLPrimitive ramType = MemoryModel.getType(ram);
            final HDLVariableDeclaration writeValue = new HDLVariableDeclaration().setType(ramType).setDirection(HDLDirection.IN);
            cachedPorts.add(writeValue.setDirection(HDLDirection.IN).setRegister(null).addVariables(createVar(CommonBusCode.WRITE_VALUE, dim, name)));

            final HDLVariableDeclaration writeEnable = new HDLVariableDeclaration().setType(HDLPrimitive.getBool()).setDirection(HDLDirection.IN);
            cachedPorts.add(writeEnable.setRegister(null).addVariables(createVar(CommonBusCode.WRITE_ENABLE, dim, name).setDefaultValue(HDLLiteral.getFalse())));

            final HDLVariableDeclaration readWriteAddress = new HDLVariableDeclaration().setType(HDLPrimitive.getNatural()).setDirection(HDLDirection.IN);
            cachedPorts.add(readWriteAddress.setRegister(null).addVariables(createVar(CommonBusCode.ADDRESS, dim, name)));

            final HDLVariableDeclaration readValue = new HDLVariableDeclaration().setType(ramType).setDirection(HDLDirection.OUT);
            cachedPorts.add(readValue.setRegister(null).addVariables(createVar(CommonBusCode.READ_VALUE, dim, name)));
        }

        public HDLVariable createVar(String string, HDLLiteral dim, String name) {
            if (dim != null) {
                return new HDLVariable().setName(name + string).addDimensions(dim);
            }
            return new HDLVariable().setName(name + string);
        }

        @Override
        protected LazyHDLInterface newInstance(int id, IHDLObject container, String name, Iterable<HDLExpression> dim, boolean validate) {
            return new LazyHDLGenerationInterface(id, container, name, dim, validate, unit, rows, context);
        }
    }

    public static HDLPrimitive getType(final BlockRam def) {
        HDLPrimitive type = null;
        switch (def.type) {
        case BIT:
            if (def.width == -1) {
                type = HDLPrimitive.getBit();
            } else {
                type = HDLPrimitive.getBitvector().setWidth(HDLLiteral.get(def.width));
            }
            break;
        case INT:
            if (def.width == -1) {
                type = HDLPrimitive.getInteger();
            } else {
                type = HDLPrimitive.getInt().setWidth(HDLLiteral.get(def.width));
            }
            break;
        case UINT:
            if (def.width == -1) {
                type = HDLPrimitive.getNatural();
            } else {
                type = HDLPrimitive.getUint().setWidth(HDLLiteral.get(def.width));
            }
            break;
        case UNUSED:
            return type;
        }
        if (type == null) {
            throw new IllegalArgumentException("Should not happen");
        }
        return type;
    }

    public static void main(String[] args) throws Exception {
        final File file = new File(args[0]);
        final Set<Problem> problems = Sets.newHashSet();
        final Unit unit = MemoryModelAST.parseUnit(Files.toString(file, Charsets.UTF_8), problems, 0);
        System.out.println(unit);
        final List<RowOrBlockRam> rows = buildRows(unit, null, null);
        final Map<String, Integer> rowCount = getRowCount(rows);
        final byte[] builtHTML = MemoryModelSideFiles.builtHTML(unit, rows, true, null, null);
        if (builtHTML == null) {
            throw new IllegalArgumentException("buildHTML returned null");
        }
        System.out.println(new BusAccess().generateAccessC(unit, "test", rows, true, rowCount));
        System.out.println(new BusAccess().generateAccessH(unit, "test", rows, true, rowCount));
        // // SideFile[] cFiles = MemoryModelSideFiles.getCFiles(unit, rows);
        // for (SideFile sideFile : cFiles) {
        // System.out.println(sideFile.relPath);
        // System.out.println(new String(sideFile.contents));
        // }
        Files.write(builtHTML, new File(args[0] + "Map.html"));
        final HDLInterface hdi = buildHDLInterface(unit, rows, null);
        System.out.println(hdi);
    }

    public static Map<String, Integer> getRowCount(final List<RowOrBlockRam> rows) {
        final Map<String, Integer> rowCount = new HashMap<>();
        for (final RowOrBlockRam row : rows) {
            Integer count = rowCount.get(row.getName());
            if (count != null) {
                count++;
            } else {
                count = 1;
            }
            rowCount.put(row.getName(), count);
        }
        return rowCount;
    }

    public static Map<String, Integer> getDefCount(final List<RowOrBlockRam> rows) {
        final Map<String, Integer> rowCount = new HashMap<>();
        for (final RowOrBlockRam block_row : rows) {
            if (block_row instanceof Row) {
                final Row row = (Row) block_row;
                for (final NamedElement ne : row.definitions) {
                    String name = ne.getName();
                    if (ne instanceof Definition) {
                        name = ((BlockRam) ne).getName(row);
                    }
                    Integer count = rowCount.get(name);
                    if (count != null) {
                        count++;
                    } else {
                        count = 1;
                    }
                    rowCount.put(name, count);
                }
            } else {
                final BlockRam ram = (BlockRam) block_row;
                Integer count = rowCount.get(ram.name);
                if (count != null) {
                    count++;
                } else {
                    count = 1;
                }
                rowCount.put(ram.name, count);
            }
        }
        return rowCount;
    }

    public static HDLInterface buildHDLInterface(Unit unit, List<RowOrBlockRam> rows, HDLEvaluationContext context) {
        return new LazyHDLGenerationInterface(unit, rows, context);
    }

    public static List<RowOrBlockRam> buildRows(Unit unit, HDLEvaluationContext context, IHDLObject container) {
        final List<RowOrBlockRam> rows = new LinkedList<>();
        for (final Object obj : unit.memory.getReferences()) {
            if (obj instanceof Reference) {
                final Reference ref = (Reference) obj;
                final Optional<NamedElement> optRes = unit.resolve(ref);
                if (!optRes.isPresent()) {
                    continue;
                }
                final NamedElement declaration = optRes.get();
                if (ref.dimensions.size() != 0) {
                    for (final String dim : ref.dimensions) {
                        final int intValue = dimToInt(context, dim, container);
                        for (int i = 0; i < intValue; i++) {
                            addDeclarations(unit, rows, declaration, null, i, context, container);
                        }
                    }
                } else {
                    addDeclarations(unit, rows, declaration, null, 0, context, container);
                }
            } else {
                addDeclarations(unit, rows, wrapConstant((Constant) obj), null, 0, context, container);
            }
        }
        final int hashCode = unit.getCheckSum();
        int biggestBlockRam = 0;
        for (final RowOrBlockRam block_row : rows) {
            if (block_row instanceof Row) {
                final Row row = (Row) block_row;
                row.updateInfo(hashCode);
            } else {
                unit.hasBlockRam = true;
                final BlockRam ram = (BlockRam) block_row;
                final int dim = nextPowerOf2(dimToInt(context, ram.dimension, container));
                biggestBlockRam = Math.max(dim, biggestBlockRam);
            }
        }
        int baseAddr = 0;
        boolean freshBlock = true;
        int rowCount = 0;
        if (biggestBlockRam == 0) {
            biggestBlockRam = Integer.MAX_VALUE;
        }
        for (final RowOrBlockRam block_row : rows) {
            block_row.setAddress(baseAddr + rowCount);
            if (block_row instanceof BlockRam) {
                baseAddr += biggestBlockRam;
                if (!freshBlock) {
                    block_row.setAddress(baseAddr);
                    baseAddr += biggestBlockRam;
                }
                rowCount = 0;
                freshBlock = true;
            } else {
                rowCount += 1;
                if (rowCount > biggestBlockRam) {
                    baseAddr += biggestBlockRam;
                    rowCount = 0;
                }
                freshBlock = false;
            }
        }
        unit.lastBase = baseAddr + rowCount;
        unit.biggestBlockRam = biggestBlockRam;
        return rows;
    }

    public static int nextPowerOf2(final int a) {
        int b = 1;
        while (b < a) {
            b = b << 1;
        }
        return b;
    }

    public static int dimToInt(HDLEvaluationContext context, final String dim, IHDLObject container) {
        int intValue;
        if (dim.charAt(0) != '$') {
            intValue = HDLLiteral.parseString(dim).intValue();
        } else {
            final String reference = dim.substring(1);
            HDLExpression hdlExpression = null;
            if (context != null) {
                hdlExpression = context.getMap().get(reference);
            }
            if (hdlExpression instanceof HDLLiteral) {
                final HDLLiteral lit = (HDLLiteral) hdlExpression;
                return lit.getValueAsBigInt().intValue();
            }
            if (hdlExpression == null) {
                hdlExpression = PSHDLParser.parseExpressionString(reference, new HashSet<>());
            }
            if (hdlExpression.getContainer() == null) {
                hdlExpression.setContainer(container.getContainer());
            }
            final BigInteger valueOf = ConstantEvaluate.valueOfForced(hdlExpression, context, "Generator");
            intValue = valueOf.intValue();
        }
        return intValue;
    }

    private static Row wrapConstant(final Constant obj) {
        final Row row = new Row(obj.getName());
        row.definitions.add(obj);
        return row;
    }

    private static void addDeclarations(Unit unit, List<RowOrBlockRam> rows, NamedElement declaration, Column parent, int colIndex, HDLEvaluationContext context,
            IHDLObject container) {
        if (declaration instanceof Column) {
            final Column col = (Column) declaration;
            for (NamedElement row : col.rows) {
                if (row instanceof Constant) {
                    row = wrapConstant((Constant) row);
                }
                addDeclarations(unit, rows, row, col, colIndex, context, container);
            }
            return;
        }
        if (declaration instanceof Row) {
            final Row row = (Row) declaration;
            final Row normalize = normalize(unit, row, context, container);
            normalize.column = parent;
            normalize.colIndex = colIndex;
            rows.add(normalize);
            return;
        }
        if (declaration instanceof Reference) {
            final Reference ref = (Reference) declaration;
            final Optional<NamedElement> optRes = unit.resolve(ref);
            if (!optRes.isPresent()) {
                throw new IllegalArgumentException("Reference not a row, column or reference:" + declaration);
            }
            final NamedElement decl = optRes.get();
            if (ref.dimensions.size() != 0) {
                for (final String dim : ref.dimensions) {
                    final int intValue = dimToInt(context, dim, container);
                    for (int i = 0; i < intValue; i++) {
                        addDeclarations(unit, rows, decl, parent, colIndex, context, container);
                    }
                }
            } else {
                addDeclarations(unit, rows, decl, parent, colIndex, context, container);
            }
            // addDeclarations(unit, rows, decl, parent, colIndex);
            return;
        }
        if (declaration instanceof BlockRam) {
            final BlockRam bram = (BlockRam) declaration;
            rows.add(bram);
            return;
        }
        throw new IllegalArgumentException("Reference not a row, column or reference:" + declaration);
    }

    private static Row normalize(Unit unit, Row row, HDLEvaluationContext context, IHDLObject container) {
        int usedSize = 0;
        boolean fillFound = false;
        final Row res = new Row(row.getOrigName());
        final List<Definition> definitions = new LinkedList<>();
        final Definition unusedFill = new Definition();
        unusedFill.name = "unused";
        for (NamedElement decl : row.definitions) {
            if (decl instanceof Reference) {
                final Reference ref = (Reference) decl;
                final Optional<NamedElement> optRes = unit.resolve(ref);
                if (!optRes.isPresent()) {
                    continue;
                }
                decl = optRes.get();
            }
            if (decl instanceof Alias) {
                final Alias alias = (Alias) decl;
                usedSize += addDeclarations(unit, definitions, alias.definitions, context, container);
            }
            if (decl instanceof Definition) {
                final Definition def = (Definition) decl;
                if ((def.type == Type.UNUSED) && (def.width < 0)) {
                    if (fillFound) {
                        throw new IllegalArgumentException("Can not have more than one fill");
                    }
                    fillFound = true;
                    definitions.add(unusedFill);
                    continue;
                }
                usedSize += handleDefinition(definitions, def, context, container);
            }
        }
        if (usedSize > Unit.rowWidth) {
            throw new IllegalArgumentException("The row:" + row.getName() + " has more bits (" + usedSize + ") than a row has bits.");
        }
        if ((usedSize != Unit.rowWidth) && (fillFound == false)) {
            throw new IllegalArgumentException("The row:" + row.getName() + " has a size of:" + usedSize + " but does not contain a fill");
        }
        if (usedSize == Unit.rowWidth) {
            definitions.remove(unusedFill);
        }
        unusedFill.width = Unit.rowWidth - usedSize;
        for (final Definition definition : definitions) {
            res.definitions.add(definition);
        }
        return res;
    }

    private static int addDeclarations(Unit unit, List<Definition> definitions, Collection<NamedElement> values, HDLEvaluationContext context, IHDLObject container) {
        int usedSize = 0;
        for (NamedElement declaration : values) {
            if (declaration instanceof Reference) {
                final Reference ref = (Reference) declaration;
                if ("fill".equals(ref.name)) {
                    throw new IllegalArgumentException("Fill not allowed in reference");
                }
                final Optional<NamedElement> optRes = unit.resolve(ref);
                if (!optRes.isPresent()) {
                    continue;
                }
                declaration = optRes.get();
            }
            if (declaration instanceof Alias) {
                final Alias alias = (Alias) declaration;
                usedSize += addDeclarations(unit, definitions, alias.definitions, context, container);
            }
            if (declaration instanceof Definition) {
                final Definition def = (Definition) declaration;
                usedSize += handleDefinition(definitions, def, context, container);
            }
        }
        return usedSize;
    }

    private static int handleDefinition(List<Definition> definitions, Definition def, HDLEvaluationContext context, IHDLObject container) {
        int usedSize = 0;
        if (def.dimension != null) {
            final int intValue = dimToInt(context, def.dimension, container);
            for (int i = 0; i < intValue; i++) {
                usedSize += getSize(def);
                final Definition withoutDim = def.withoutDim();
                definitions.add(withoutDim);
            }
        } else {
            usedSize += getSize(def);
            definitions.add(def);
        }
        return usedSize;
    }

    public static int getSize(BlockRam def) {
        if (def.width == -1) {
            switch (def.type) {
            case BIT:
                return 1;
            case INT:
            case UINT:
                return 32;
            default:
            }
        }
        return def.width;
    }

}
