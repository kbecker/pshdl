/*******************************************************************************
 * PSHDL is a library and (trans-)compiler for PSHDL input. It generates
 *     output suitable for implementation or simulation of it.
 *
 *     Copyright (C) 2013 Karsten Becker (feedback (at) pshdl (dot) org)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *     This License does not grant permission to use the trade names, trademarks,
 *     service marks, or product names of the Licensor, except as required for
 *     reasonable and customary use in describing the origin of the Work.
 *
 * Contributors:
 *     Karsten Becker - initial API and implementation
 ******************************************************************************/
// Generated from MemoryModel.g4 by ANTLR 4.7.1
package org.pshdl.model.types.builtIn.busses.memorymodel.v4;

import java.util.List;

import org.antlr.v4.runtime.NoViableAltException;
import org.antlr.v4.runtime.Parser;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.RuntimeMetaData;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.Vocabulary;
import org.antlr.v4.runtime.VocabularyImpl;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.ParserATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.TerminalNode;

@SuppressWarnings({ "all", "warnings", "unchecked", "unused", "cast" })
public class MemoryModelParser extends Parser {
    static {
        RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION);
    }

    protected static final DFA[] _decisionToDFA;
    protected static final PredictionContextCache _sharedContextCache = new PredictionContextCache();
    public static final int T__0 = 1, T__1 = 2, T__2 = 3, T__3 = 4, T__4 = 5, T__5 = 6, T__6 = 7, T__7 = 8, T__8 = 9, T__9 = 10, T__10 = 11, T__11 = 12, T__12 = 13, T__13 = 14,
            T__14 = 15, T__15 = 16, T__16 = 17, T__17 = 18, T__18 = 19, T__19 = 20, T__20 = 21, T__21 = 22, T__22 = 23, T__23 = 24, T__24 = 25, T__25 = 26, T__26 = 27, T__27 = 28,
            T__28 = 29, T__29 = 30, ID = 31, NUMBER = 32, COMMENT = 33, WS = 34;
    public static final int RULE_unit = 0, RULE_declaration = 1, RULE_blockRam = 2, RULE_row = 3, RULE_rowID = 4, RULE_constant = 5, RULE_filling = 6, RULE_column = 7,
            RULE_alias = 8, RULE_memory = 9, RULE_definition = 10, RULE_warnType = 11, RULE_rwStatus = 12, RULE_array = 13, RULE_extNumber = 14, RULE_extRef = 15, RULE_type = 16,
            RULE_reference = 17;
    public static final String[] ruleNames = { "unit", "declaration", "blockRam", "row", "rowID", "constant", "filling", "column", "alias", "memory", "definition", "warnType",
            "rwStatus", "array", "extNumber", "extRef", "type", "reference" };

    private static final String[] _LITERAL_NAMES = { null, "'blockRam'", "'<'", "'>'", "'['", "']'", "'readFlag'", "'writtenFlag'", "';'", "'row'", "'{'", "'}'", "'^'", "'const'",
            "'fill'", "'column'", "'alias'", "'memory'", "'register'", "'='", "'silent'", "'mask'", "'error'", "'limit'", "'r'", "'w'", "'rw'", "'$'", "'int'", "'uint'", "'bit'" };
    private static final String[] _SYMBOLIC_NAMES = { null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
            null, null, null, null, null, null, null, null, null, null, "ID", "NUMBER", "COMMENT", "WS" };
    public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

    /**
     * @deprecated Use {@link #VOCABULARY} instead.
     */
    @Deprecated
    public static final String[] tokenNames;
    static {
        tokenNames = new String[_SYMBOLIC_NAMES.length];
        for (int i = 0; i < tokenNames.length; i++) {
            tokenNames[i] = VOCABULARY.getLiteralName(i);
            if (tokenNames[i] == null) {
                tokenNames[i] = VOCABULARY.getSymbolicName(i);
            }

            if (tokenNames[i] == null) {
                tokenNames[i] = "<INVALID>";
            }
        }
    }

    @Override
    @Deprecated
    public String[] getTokenNames() {
        return tokenNames;
    }

    @Override

    public Vocabulary getVocabulary() {
        return VOCABULARY;
    }

    @Override
    public String getGrammarFileName() {
        return "MemoryModel.g4";
    }

    @Override
    public String[] getRuleNames() {
        return ruleNames;
    }

    @Override
    public String getSerializedATN() {
        return _serializedATN;
    }

    @Override
    public ATN getATN() {
        return _ATN;
    }

    public MemoryModelParser(TokenStream input) {
        super(input);
        _interp = new ParserATNSimulator(this, _ATN, _decisionToDFA, _sharedContextCache);
    }

    public static class UnitContext extends ParserRuleContext {
        public MemoryContext memory() {
            return getRuleContext(MemoryContext.class, 0);
        }

        public List<DeclarationContext> declaration() {
            return getRuleContexts(DeclarationContext.class);
        }

        public DeclarationContext declaration(int i) {
            return getRuleContext(DeclarationContext.class, i);
        }

        public UnitContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_unit;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).enterUnit(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).exitUnit(this);
            }
        }
    }

    public final UnitContext unit() throws RecognitionException {
        final UnitContext _localctx = new UnitContext(_ctx, getState());
        enterRule(_localctx, 0, RULE_unit);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(39);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (((((_la) & ~0x3f) == 0) && (((1L << _la) & ((1L << T__8) | (1L << T__14) | (1L << T__15))) != 0))) {
                    {
                        {
                            setState(36);
                            declaration();
                        }
                    }
                    setState(41);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
                {
                    setState(42);
                    memory();
                }
            }
        } catch (final RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class DeclarationContext extends ParserRuleContext {
        public RowContext row() {
            return getRuleContext(RowContext.class, 0);
        }

        public ColumnContext column() {
            return getRuleContext(ColumnContext.class, 0);
        }

        public AliasContext alias() {
            return getRuleContext(AliasContext.class, 0);
        }

        public DeclarationContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_declaration;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).enterDeclaration(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).exitDeclaration(this);
            }
        }
    }

    public final DeclarationContext declaration() throws RecognitionException {
        final DeclarationContext _localctx = new DeclarationContext(_ctx, getState());
        enterRule(_localctx, 2, RULE_declaration);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(47);
                _errHandler.sync(this);
                switch (_input.LA(1)) {
                case T__8: {
                    {
                        setState(44);
                        row();
                    }
                }
                    break;
                case T__14: {
                    {
                        setState(45);
                        column();
                    }
                }
                    break;
                case T__15: {
                    {
                        setState(46);
                        alias();
                    }
                }
                    break;
                default:
                    throw new NoViableAltException(this);
                }
            }
        } catch (final RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class BlockRamContext extends ParserRuleContext {
        public ExtNumberContext width;
        public Token readFlag;
        public Token writtenFlag;

        public RwStatusContext rwStatus() {
            return getRuleContext(RwStatusContext.class, 0);
        }

        public TypeContext type() {
            return getRuleContext(TypeContext.class, 0);
        }

        public TerminalNode ID() {
            return getToken(MemoryModelParser.ID, 0);
        }

        public ArrayContext array() {
            return getRuleContext(ArrayContext.class, 0);
        }

        public ExtNumberContext extNumber() {
            return getRuleContext(ExtNumberContext.class, 0);
        }

        public BlockRamContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_blockRam;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).enterBlockRam(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).exitBlockRam(this);
            }
        }
    }

    public final BlockRamContext blockRam() throws RecognitionException {
        final BlockRamContext _localctx = new BlockRamContext(_ctx, getState());
        enterRule(_localctx, 4, RULE_blockRam);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(49);
                rwStatus();
                setState(50);
                match(T__0);
                setState(51);
                type();
                setState(56);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__1) {
                    {
                        setState(52);
                        match(T__1);
                        setState(53);
                        _localctx.width = extNumber();
                        setState(54);
                        match(T__2);
                    }
                }

                setState(58);
                match(ID);
                setState(63);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__3) {
                    {
                        setState(59);
                        match(T__3);
                        setState(60);
                        array();
                        setState(61);
                        match(T__4);
                    }
                }

                setState(66);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__5) {
                    {
                        setState(65);
                        _localctx.readFlag = match(T__5);
                    }
                }

                setState(69);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__6) {
                    {
                        setState(68);
                        _localctx.writtenFlag = match(T__6);
                    }
                }

                setState(71);
                match(T__7);
            }
        } catch (final RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class RowContext extends ParserRuleContext {
        public RowIDContext rowID() {
            return getRuleContext(RowIDContext.class, 0);
        }

        public List<FillingContext> filling() {
            return getRuleContexts(FillingContext.class);
        }

        public FillingContext filling(int i) {
            return getRuleContext(FillingContext.class, i);
        }

        public List<DefinitionContext> definition() {
            return getRuleContexts(DefinitionContext.class);
        }

        public DefinitionContext definition(int i) {
            return getRuleContext(DefinitionContext.class, i);
        }

        public List<ReferenceContext> reference() {
            return getRuleContexts(ReferenceContext.class);
        }

        public ReferenceContext reference(int i) {
            return getRuleContext(ReferenceContext.class, i);
        }

        public List<ConstantContext> constant() {
            return getRuleContexts(ConstantContext.class);
        }

        public ConstantContext constant(int i) {
            return getRuleContext(ConstantContext.class, i);
        }

        public RowContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_row;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).enterRow(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).exitRow(this);
            }
        }
    }

    public final RowContext row() throws RecognitionException {
        final RowContext _localctx = new RowContext(_ctx, getState());
        enterRule(_localctx, 6, RULE_row);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(73);
                match(T__8);
                setState(74);
                rowID();
                setState(75);
                match(T__9);
                setState(82);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (((((_la) & ~0x3f) == 0) && (((1L << _la) & ((1L << T__12) | (1L << T__13) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << ID))) != 0))) {
                    {
                        setState(80);
                        _errHandler.sync(this);
                        switch (_input.LA(1)) {
                        case T__13: {
                            {
                                setState(76);
                                filling();
                            }
                        }
                            break;
                        case T__23:
                        case T__24:
                        case T__25: {
                            {
                                setState(77);
                                definition();
                            }
                        }
                            break;
                        case ID: {
                            {
                                setState(78);
                                reference();
                            }
                        }
                            break;
                        case T__12: {
                            {
                                setState(79);
                                constant();
                            }
                        }
                            break;
                        default:
                            throw new NoViableAltException(this);
                        }
                    }
                    setState(84);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
                setState(85);
                match(T__10);
            }
        } catch (final RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class RowIDContext extends ParserRuleContext {
        public TerminalNode ID() {
            return getToken(MemoryModelParser.ID, 0);
        }

        public RowIDContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_rowID;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).enterRowID(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).exitRowID(this);
            }
        }
    }

    public final RowIDContext rowID() throws RecognitionException {
        final RowIDContext _localctx = new RowIDContext(_ctx, getState());
        enterRule(_localctx, 8, RULE_rowID);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(88);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__11) {
                    {
                        setState(87);
                        match(T__11);
                    }
                }

                setState(90);
                match(ID);
            }
        } catch (final RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ConstantContext extends ParserRuleContext {
        public ExtNumberContext width;
        public ExtNumberContext value;

        public List<ExtNumberContext> extNumber() {
            return getRuleContexts(ExtNumberContext.class);
        }

        public ExtNumberContext extNumber(int i) {
            return getRuleContext(ExtNumberContext.class, i);
        }

        public TerminalNode ID() {
            return getToken(MemoryModelParser.ID, 0);
        }

        public ConstantContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_constant;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).enterConstant(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).exitConstant(this);
            }
        }
    }

    public final ConstantContext constant() throws RecognitionException {
        final ConstantContext _localctx = new ConstantContext(_ctx, getState());
        enterRule(_localctx, 10, RULE_constant);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(92);
                match(T__12);
                setState(97);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__1) {
                    {
                        setState(93);
                        match(T__1);
                        setState(94);
                        _localctx.width = extNumber();
                        setState(95);
                        match(T__2);
                    }
                }

                setState(100);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == ID) {
                    {
                        setState(99);
                        match(ID);
                    }
                }

                setState(102);
                _localctx.value = extNumber();
                setState(103);
                match(T__7);
            }
        } catch (final RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class FillingContext extends ParserRuleContext {
        public ExtNumberContext width;

        public ExtNumberContext extNumber() {
            return getRuleContext(ExtNumberContext.class, 0);
        }

        public FillingContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_filling;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).enterFilling(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).exitFilling(this);
            }
        }
    }

    public final FillingContext filling() throws RecognitionException {
        final FillingContext _localctx = new FillingContext(_ctx, getState());
        enterRule(_localctx, 12, RULE_filling);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(105);
                match(T__13);
                setState(110);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__1) {
                    {
                        setState(106);
                        match(T__1);
                        setState(107);
                        _localctx.width = extNumber();
                        setState(108);
                        match(T__2);
                    }
                }

                setState(112);
                match(T__7);
            }
        } catch (final RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ColumnContext extends ParserRuleContext {
        public TerminalNode ID() {
            return getToken(MemoryModelParser.ID, 0);
        }

        public List<ReferenceContext> reference() {
            return getRuleContexts(ReferenceContext.class);
        }

        public ReferenceContext reference(int i) {
            return getRuleContext(ReferenceContext.class, i);
        }

        public List<ConstantContext> constant() {
            return getRuleContexts(ConstantContext.class);
        }

        public ConstantContext constant(int i) {
            return getRuleContext(ConstantContext.class, i);
        }

        public List<BlockRamContext> blockRam() {
            return getRuleContexts(BlockRamContext.class);
        }

        public BlockRamContext blockRam(int i) {
            return getRuleContext(BlockRamContext.class, i);
        }

        public ColumnContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_column;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).enterColumn(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).exitColumn(this);
            }
        }
    }

    public final ColumnContext column() throws RecognitionException {
        final ColumnContext _localctx = new ColumnContext(_ctx, getState());
        enterRule(_localctx, 14, RULE_column);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(114);
                match(T__14);
                setState(115);
                match(ID);
                setState(116);
                match(T__9);
                setState(122);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (((((_la) & ~0x3f) == 0) && (((1L << _la) & ((1L << T__12) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << ID))) != 0))) {
                    {
                        setState(120);
                        _errHandler.sync(this);
                        switch (_input.LA(1)) {
                        case ID: {
                            {
                                setState(117);
                                reference();
                            }
                        }
                            break;
                        case T__12: {
                            {
                                setState(118);
                                constant();
                            }
                        }
                            break;
                        case T__23:
                        case T__24:
                        case T__25: {
                            {
                                setState(119);
                                blockRam();
                            }
                        }
                            break;
                        default:
                            throw new NoViableAltException(this);
                        }
                    }
                    setState(124);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
                setState(125);
                match(T__10);
            }
        } catch (final RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class AliasContext extends ParserRuleContext {
        public TerminalNode ID() {
            return getToken(MemoryModelParser.ID, 0);
        }

        public List<DefinitionContext> definition() {
            return getRuleContexts(DefinitionContext.class);
        }

        public DefinitionContext definition(int i) {
            return getRuleContext(DefinitionContext.class, i);
        }

        public List<ReferenceContext> reference() {
            return getRuleContexts(ReferenceContext.class);
        }

        public ReferenceContext reference(int i) {
            return getRuleContext(ReferenceContext.class, i);
        }

        public AliasContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_alias;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).enterAlias(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).exitAlias(this);
            }
        }
    }

    public final AliasContext alias() throws RecognitionException {
        final AliasContext _localctx = new AliasContext(_ctx, getState());
        enterRule(_localctx, 16, RULE_alias);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(127);
                match(T__15);
                setState(128);
                match(ID);
                setState(129);
                match(T__9);
                setState(134);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (((((_la) & ~0x3f) == 0) && (((1L << _la) & ((1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << ID))) != 0))) {
                    {
                        setState(132);
                        _errHandler.sync(this);
                        switch (_input.LA(1)) {
                        case T__23:
                        case T__24:
                        case T__25: {
                            {
                                setState(130);
                                definition();
                            }
                        }
                            break;
                        case ID: {
                            {
                                setState(131);
                                reference();
                            }
                        }
                            break;
                        default:
                            throw new NoViableAltException(this);
                        }
                    }
                    setState(136);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
                setState(137);
                match(T__10);
            }
        } catch (final RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class MemoryContext extends ParserRuleContext {
        public List<ReferenceContext> reference() {
            return getRuleContexts(ReferenceContext.class);
        }

        public ReferenceContext reference(int i) {
            return getRuleContext(ReferenceContext.class, i);
        }

        public List<ConstantContext> constant() {
            return getRuleContexts(ConstantContext.class);
        }

        public ConstantContext constant(int i) {
            return getRuleContext(ConstantContext.class, i);
        }

        public List<BlockRamContext> blockRam() {
            return getRuleContexts(BlockRamContext.class);
        }

        public BlockRamContext blockRam(int i) {
            return getRuleContext(BlockRamContext.class, i);
        }

        public MemoryContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_memory;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).enterMemory(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).exitMemory(this);
            }
        }
    }

    public final MemoryContext memory() throws RecognitionException {
        final MemoryContext _localctx = new MemoryContext(_ctx, getState());
        enterRule(_localctx, 18, RULE_memory);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(139);
                match(T__16);
                setState(140);
                match(T__9);
                setState(146);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (((((_la) & ~0x3f) == 0) && (((1L << _la) & ((1L << T__12) | (1L << T__23) | (1L << T__24) | (1L << T__25) | (1L << ID))) != 0))) {
                    {
                        setState(144);
                        _errHandler.sync(this);
                        switch (_input.LA(1)) {
                        case ID: {
                            {
                                setState(141);
                                reference();
                            }
                        }
                            break;
                        case T__12: {
                            {
                                setState(142);
                                constant();
                            }
                        }
                            break;
                        case T__23:
                        case T__24:
                        case T__25: {
                            {
                                setState(143);
                                blockRam();
                            }
                        }
                            break;
                        default:
                            throw new NoViableAltException(this);
                        }
                    }
                    setState(148);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
                setState(149);
                match(T__10);
            }
        } catch (final RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class DefinitionContext extends ParserRuleContext {
        public Token hasRegister;
        public ExtNumberContext width;
        public ExtNumberContext resetValue;
        public Token readFlag;
        public Token writtenFlag;

        public RwStatusContext rwStatus() {
            return getRuleContext(RwStatusContext.class, 0);
        }

        public TypeContext type() {
            return getRuleContext(TypeContext.class, 0);
        }

        public TerminalNode ID() {
            return getToken(MemoryModelParser.ID, 0);
        }

        public ArrayContext array() {
            return getRuleContext(ArrayContext.class, 0);
        }

        public WarnTypeContext warnType() {
            return getRuleContext(WarnTypeContext.class, 0);
        }

        public List<ExtNumberContext> extNumber() {
            return getRuleContexts(ExtNumberContext.class);
        }

        public ExtNumberContext extNumber(int i) {
            return getRuleContext(ExtNumberContext.class, i);
        }

        public DefinitionContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_definition;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).enterDefinition(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).exitDefinition(this);
            }
        }
    }

    public final DefinitionContext definition() throws RecognitionException {
        final DefinitionContext _localctx = new DefinitionContext(_ctx, getState());
        enterRule(_localctx, 20, RULE_definition);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(151);
                rwStatus();
                setState(153);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__17) {
                    {
                        setState(152);
                        _localctx.hasRegister = match(T__17);
                    }
                }

                setState(155);
                type();
                setState(160);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__1) {
                    {
                        setState(156);
                        match(T__1);
                        setState(157);
                        _localctx.width = extNumber();
                        setState(158);
                        match(T__2);
                    }
                }

                setState(162);
                match(ID);
                setState(165);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__18) {
                    {
                        setState(163);
                        match(T__18);
                        setState(164);
                        _localctx.resetValue = extNumber();
                    }
                }

                setState(171);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__3) {
                    {
                        setState(167);
                        match(T__3);
                        setState(168);
                        array();
                        setState(169);
                        match(T__4);
                    }
                }

                setState(174);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (((((_la) & ~0x3f) == 0) && (((1L << _la) & ((1L << T__19) | (1L << T__20) | (1L << T__21) | (1L << T__22))) != 0))) {
                    {
                        setState(173);
                        warnType();
                    }
                }

                setState(177);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__5) {
                    {
                        setState(176);
                        _localctx.readFlag = match(T__5);
                    }
                }

                setState(180);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__6) {
                    {
                        setState(179);
                        _localctx.writtenFlag = match(T__6);
                    }
                }

                setState(182);
                match(T__7);
            }
        } catch (final RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class WarnTypeContext extends ParserRuleContext {
        public Token silent;
        public Token typeString;

        public WarnTypeContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_warnType;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).enterWarnType(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).exitWarnType(this);
            }
        }
    }

    public final WarnTypeContext warnType() throws RecognitionException {
        final WarnTypeContext _localctx = new WarnTypeContext(_ctx, getState());
        enterRule(_localctx, 22, RULE_warnType);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(185);
                _errHandler.sync(this);
                _la = _input.LA(1);
                if (_la == T__19) {
                    {
                        setState(184);
                        _localctx.silent = match(T__19);
                    }
                }

                setState(187);
                _localctx.typeString = _input.LT(1);
                _la = _input.LA(1);
                if (!(((((_la) & ~0x3f) == 0) && (((1L << _la) & ((1L << T__20) | (1L << T__21) | (1L << T__22))) != 0)))) {
                    _localctx.typeString = _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF) {
                        matchedEOF = true;
                    }
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (final RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class RwStatusContext extends ParserRuleContext {
        public RwStatusContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_rwStatus;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).enterRwStatus(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).exitRwStatus(this);
            }
        }
    }

    public final RwStatusContext rwStatus() throws RecognitionException {
        final RwStatusContext _localctx = new RwStatusContext(_ctx, getState());
        enterRule(_localctx, 24, RULE_rwStatus);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(189);
                _la = _input.LA(1);
                if (!(((((_la) & ~0x3f) == 0) && (((1L << _la) & ((1L << T__23) | (1L << T__24) | (1L << T__25))) != 0)))) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF) {
                        matchedEOF = true;
                    }
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (final RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ArrayContext extends ParserRuleContext {
        public ExtNumberContext extNumber() {
            return getRuleContext(ExtNumberContext.class, 0);
        }

        public ArrayContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_array;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).enterArray(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).exitArray(this);
            }
        }
    }

    public final ArrayContext array() throws RecognitionException {
        final ArrayContext _localctx = new ArrayContext(_ctx, getState());
        enterRule(_localctx, 26, RULE_array);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(191);
                extNumber();
            }
        } catch (final RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ExtNumberContext extends ParserRuleContext {
        public TerminalNode NUMBER() {
            return getToken(MemoryModelParser.NUMBER, 0);
        }

        public ExtRefContext extRef() {
            return getRuleContext(ExtRefContext.class, 0);
        }

        public ExtNumberContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_extNumber;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).enterExtNumber(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).exitExtNumber(this);
            }
        }
    }

    public final ExtNumberContext extNumber() throws RecognitionException {
        final ExtNumberContext _localctx = new ExtNumberContext(_ctx, getState());
        enterRule(_localctx, 28, RULE_extNumber);
        try {
            setState(195);
            _errHandler.sync(this);
            switch (_input.LA(1)) {
            case NUMBER:
                enterOuterAlt(_localctx, 1); {
                setState(193);
                match(NUMBER);
            }
                break;
            case T__26:
                enterOuterAlt(_localctx, 2); {
                setState(194);
                extRef();
            }
                break;
            default:
                throw new NoViableAltException(this);
            }
        } catch (final RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ExtRefContext extends ParserRuleContext {
        public TerminalNode ID() {
            return getToken(MemoryModelParser.ID, 0);
        }

        public ExtRefContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_extRef;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).enterExtRef(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).exitExtRef(this);
            }
        }
    }

    public final ExtRefContext extRef() throws RecognitionException {
        final ExtRefContext _localctx = new ExtRefContext(_ctx, getState());
        enterRule(_localctx, 30, RULE_extRef);
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(197);
                match(T__26);
                setState(198);
                match(ID);
            }
        } catch (final RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class TypeContext extends ParserRuleContext {
        public TypeContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_type;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).enterType(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).exitType(this);
            }
        }
    }

    public final TypeContext type() throws RecognitionException {
        final TypeContext _localctx = new TypeContext(_ctx, getState());
        enterRule(_localctx, 32, RULE_type);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(200);
                _la = _input.LA(1);
                if (!(((((_la) & ~0x3f) == 0) && (((1L << _la) & ((1L << T__27) | (1L << T__28) | (1L << T__29))) != 0)))) {
                    _errHandler.recoverInline(this);
                } else {
                    if (_input.LA(1) == Token.EOF) {
                        matchedEOF = true;
                    }
                    _errHandler.reportMatch(this);
                    consume();
                }
            }
        } catch (final RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static class ReferenceContext extends ParserRuleContext {
        public TerminalNode ID() {
            return getToken(MemoryModelParser.ID, 0);
        }

        public List<ArrayContext> array() {
            return getRuleContexts(ArrayContext.class);
        }

        public ArrayContext array(int i) {
            return getRuleContext(ArrayContext.class, i);
        }

        public ReferenceContext(ParserRuleContext parent, int invokingState) {
            super(parent, invokingState);
        }

        @Override
        public int getRuleIndex() {
            return RULE_reference;
        }

        @Override
        public void enterRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).enterReference(this);
            }
        }

        @Override
        public void exitRule(ParseTreeListener listener) {
            if (listener instanceof MemoryModelListener) {
                ((MemoryModelListener) listener).exitReference(this);
            }
        }
    }

    public final ReferenceContext reference() throws RecognitionException {
        final ReferenceContext _localctx = new ReferenceContext(_ctx, getState());
        enterRule(_localctx, 34, RULE_reference);
        int _la;
        try {
            enterOuterAlt(_localctx, 1);
            {
                setState(202);
                match(ID);
                setState(209);
                _errHandler.sync(this);
                _la = _input.LA(1);
                while (_la == T__3) {
                    {
                        {
                            setState(203);
                            match(T__3);
                            setState(204);
                            array();
                            setState(205);
                            match(T__4);
                        }
                    }
                    setState(211);
                    _errHandler.sync(this);
                    _la = _input.LA(1);
                }
                setState(212);
                match(T__7);
            }
        } catch (final RecognitionException re) {
            _localctx.exception = re;
            _errHandler.reportError(this, re);
            _errHandler.recover(this, re);
        } finally {
            exitRule();
        }
        return _localctx;
    }

    public static final String _serializedATN = "\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3$\u00d9\4\2\t\2\4"
            + "\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t" + "\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"
            + "\4\23\t\23\3\2\7\2(\n\2\f\2\16\2+\13\2\3\2\3\2\3\3\3\3\3\3\5\3\62\n\3" + "\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4;\n\4\3\4\3\4\3\4\3\4\3\4\5\4B\n\4\3\4"
            + "\5\4E\n\4\3\4\5\4H\n\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\7\5S\n\5\f" + "\5\16\5V\13\5\3\5\3\5\3\6\5\6[\n\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\5\7d\n"
            + "\7\3\7\5\7g\n\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\5\bq\n\b\3\b\3\b\3\t\3" + "\t\3\t\3\t\3\t\3\t\7\t{\n\t\f\t\16\t~\13\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n"
            + "\7\n\u0087\n\n\f\n\16\n\u008a\13\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\7" + "\13\u0093\n\13\f\13\16\13\u0096\13\13\3\13\3\13\3\f\3\f\5\f\u009c\n\f"
            + "\3\f\3\f\3\f\3\f\3\f\5\f\u00a3\n\f\3\f\3\f\3\f\5\f\u00a8\n\f\3\f\3\f\3" + "\f\3\f\5\f\u00ae\n\f\3\f\5\f\u00b1\n\f\3\f\5\f\u00b4\n\f\3\f\5\f\u00b7"
            + "\n\f\3\f\3\f\3\r\5\r\u00bc\n\r\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\5" + "\20\u00c6\n\20\3\21\3\21\3\21\3\22\3\22\3\23\3\23\3\23\3\23\3\23\7\23"
            + "\u00d2\n\23\f\23\16\23\u00d5\13\23\3\23\3\23\3\23\2\2\24\2\4\6\b\n\f\16" + "\20\22\24\26\30\32\34\36 \"$\2\5\3\2\27\31\3\2\32\34\3\2\36 \2\u00e7\2"
            + ")\3\2\2\2\4\61\3\2\2\2\6\63\3\2\2\2\bK\3\2\2\2\nZ\3\2\2\2\f^\3\2\2\2\16" + "k\3\2\2\2\20t\3\2\2\2\22\u0081\3\2\2\2\24\u008d\3\2\2\2\26\u0099\3\2\2"
            + "\2\30\u00bb\3\2\2\2\32\u00bf\3\2\2\2\34\u00c1\3\2\2\2\36\u00c5\3\2\2\2" + " \u00c7\3\2\2\2\"\u00ca\3\2\2\2$\u00cc\3\2\2\2&(\5\4\3\2\'&\3\2\2\2(+"
            + "\3\2\2\2)\'\3\2\2\2)*\3\2\2\2*,\3\2\2\2+)\3\2\2\2,-\5\24\13\2-\3\3\2\2" + "\2.\62\5\b\5\2/\62\5\20\t\2\60\62\5\22\n\2\61.\3\2\2\2\61/\3\2\2\2\61"
            + "\60\3\2\2\2\62\5\3\2\2\2\63\64\5\32\16\2\64\65\7\3\2\2\65:\5\"\22\2\66" + "\67\7\4\2\2\678\5\36\20\289\7\5\2\29;\3\2\2\2:\66\3\2\2\2:;\3\2\2\2;<"
            + "\3\2\2\2<A\7!\2\2=>\7\6\2\2>?\5\34\17\2?@\7\7\2\2@B\3\2\2\2A=\3\2\2\2" + "AB\3\2\2\2BD\3\2\2\2CE\7\b\2\2DC\3\2\2\2DE\3\2\2\2EG\3\2\2\2FH\7\t\2\2"
            + "GF\3\2\2\2GH\3\2\2\2HI\3\2\2\2IJ\7\n\2\2J\7\3\2\2\2KL\7\13\2\2LM\5\n\6" + "\2MT\7\f\2\2NS\5\16\b\2OS\5\26\f\2PS\5$\23\2QS\5\f\7\2RN\3\2\2\2RO\3\2"
            + "\2\2RP\3\2\2\2RQ\3\2\2\2SV\3\2\2\2TR\3\2\2\2TU\3\2\2\2UW\3\2\2\2VT\3\2" + "\2\2WX\7\r\2\2X\t\3\2\2\2Y[\7\16\2\2ZY\3\2\2\2Z[\3\2\2\2[\\\3\2\2\2\\"
            + "]\7!\2\2]\13\3\2\2\2^c\7\17\2\2_`\7\4\2\2`a\5\36\20\2ab\7\5\2\2bd\3\2" + "\2\2c_\3\2\2\2cd\3\2\2\2df\3\2\2\2eg\7!\2\2fe\3\2\2\2fg\3\2\2\2gh\3\2"
            + "\2\2hi\5\36\20\2ij\7\n\2\2j\r\3\2\2\2kp\7\20\2\2lm\7\4\2\2mn\5\36\20\2" + "no\7\5\2\2oq\3\2\2\2pl\3\2\2\2pq\3\2\2\2qr\3\2\2\2rs\7\n\2\2s\17\3\2\2"
            + "\2tu\7\21\2\2uv\7!\2\2v|\7\f\2\2w{\5$\23\2x{\5\f\7\2y{\5\6\4\2zw\3\2\2" + "\2zx\3\2\2\2zy\3\2\2\2{~\3\2\2\2|z\3\2\2\2|}\3\2\2\2}\177\3\2\2\2~|\3"
            + "\2\2\2\177\u0080\7\r\2\2\u0080\21\3\2\2\2\u0081\u0082\7\22\2\2\u0082\u0083" + "\7!\2\2\u0083\u0088\7\f\2\2\u0084\u0087\5\26\f\2\u0085\u0087\5$\23\2\u0086"
            + "\u0084\3\2\2\2\u0086\u0085\3\2\2\2\u0087\u008a\3\2\2\2\u0088\u0086\3\2" + "\2\2\u0088\u0089\3\2\2\2\u0089\u008b\3\2\2\2\u008a\u0088\3\2\2\2\u008b"
            + "\u008c\7\r\2\2\u008c\23\3\2\2\2\u008d\u008e\7\23\2\2\u008e\u0094\7\f\2" + "\2\u008f\u0093\5$\23\2\u0090\u0093\5\f\7\2\u0091\u0093\5\6\4\2\u0092\u008f"
            + "\3\2\2\2\u0092\u0090\3\2\2\2\u0092\u0091\3\2\2\2\u0093\u0096\3\2\2\2\u0094" + "\u0092\3\2\2\2\u0094\u0095\3\2\2\2\u0095\u0097\3\2\2\2\u0096\u0094\3\2"
            + "\2\2\u0097\u0098\7\r\2\2\u0098\25\3\2\2\2\u0099\u009b\5\32\16\2\u009a" + "\u009c\7\24\2\2\u009b\u009a\3\2\2\2\u009b\u009c\3\2\2\2\u009c\u009d\3"
            + "\2\2\2\u009d\u00a2\5\"\22\2\u009e\u009f\7\4\2\2\u009f\u00a0\5\36\20\2" + "\u00a0\u00a1\7\5\2\2\u00a1\u00a3\3\2\2\2\u00a2\u009e\3\2\2\2\u00a2\u00a3"
            + "\3\2\2\2\u00a3\u00a4\3\2\2\2\u00a4\u00a7\7!\2\2\u00a5\u00a6\7\25\2\2\u00a6" + "\u00a8\5\36\20\2\u00a7\u00a5\3\2\2\2\u00a7\u00a8\3\2\2\2\u00a8\u00ad\3"
            + "\2\2\2\u00a9\u00aa\7\6\2\2\u00aa\u00ab\5\34\17\2\u00ab\u00ac\7\7\2\2\u00ac" + "\u00ae\3\2\2\2\u00ad\u00a9\3\2\2\2\u00ad\u00ae\3\2\2\2\u00ae\u00b0\3\2"
            + "\2\2\u00af\u00b1\5\30\r\2\u00b0\u00af\3\2\2\2\u00b0\u00b1\3\2\2\2\u00b1" + "\u00b3\3\2\2\2\u00b2\u00b4\7\b\2\2\u00b3\u00b2\3\2\2\2\u00b3\u00b4\3\2"
            + "\2\2\u00b4\u00b6\3\2\2\2\u00b5\u00b7\7\t\2\2\u00b6\u00b5\3\2\2\2\u00b6" + "\u00b7\3\2\2\2\u00b7\u00b8\3\2\2\2\u00b8\u00b9\7\n\2\2\u00b9\27\3\2\2"
            + "\2\u00ba\u00bc\7\26\2\2\u00bb\u00ba\3\2\2\2\u00bb\u00bc\3\2\2\2\u00bc" + "\u00bd\3\2\2\2\u00bd\u00be\t\2\2\2\u00be\31\3\2\2\2\u00bf\u00c0\t\3\2"
            + "\2\u00c0\33\3\2\2\2\u00c1\u00c2\5\36\20\2\u00c2\35\3\2\2\2\u00c3\u00c6" + "\7\"\2\2\u00c4\u00c6\5 \21\2\u00c5\u00c3\3\2\2\2\u00c5\u00c4\3\2\2\2\u00c6"
            + "\37\3\2\2\2\u00c7\u00c8\7\35\2\2\u00c8\u00c9\7!\2\2\u00c9!\3\2\2\2\u00ca" + "\u00cb\t\4\2\2\u00cb#\3\2\2\2\u00cc\u00d3\7!\2\2\u00cd\u00ce\7\6\2\2\u00ce"
            + "\u00cf\5\34\17\2\u00cf\u00d0\7\7\2\2\u00d0\u00d2\3\2\2\2\u00d1\u00cd\3" + "\2\2\2\u00d2\u00d5\3\2\2\2\u00d3\u00d1\3\2\2\2\u00d3\u00d4\3\2\2\2\u00d4"
            + "\u00d6\3\2\2\2\u00d5\u00d3\3\2\2\2\u00d6\u00d7\7\n\2\2\u00d7%\3\2\2\2" + "\36)\61:ADGRTZcfpz|\u0086\u0088\u0092\u0094\u009b\u00a2\u00a7\u00ad\u00b0"
            + "\u00b3\u00b6\u00bb\u00c5\u00d3";
    public static final ATN _ATN = new ATNDeserializer().deserialize(_serializedATN.toCharArray());
    static {
        _decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
        for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
            _decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
        }
    }
}